<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 01/04/17
 * Time: 18:22
 */

namespace Tests\AppBundle\Controller\Api;

interface ABOTestFixture {

    public static function loadFixtures();
}
