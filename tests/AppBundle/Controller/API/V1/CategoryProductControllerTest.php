<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryProduct;
use AppBundle\Entity\GeneralSpec;
use AppBundle\Entity\Image;
use AppBundle\Entity\ImageProduct;
use AppBundle\Entity\Product;
use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use GuzzleHttp\Client;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class CategoryProductControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/categoryproducts';
    private static $id;
    private static $slug;
    private static $catId;
    private static $shopId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [ 'id', 'product', 'category', 'general_specs', 'rate_stat',
        'image', 'shop', 'any_parent', 'parent' ];

    public function testGetCategoryProduct() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetCategoryProductBySlug() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$slug );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetCategoryProducts() {

        $response = $this->client->get( $this->uriPrefix );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewCategoryProduct() {

        $data = [
            'product' => [
                'reference' => 'prod-4578',
                'name' => 'Product Celio',
                'mark' => 'Celio Edited',
                'public' => false,
            ],
            'category' => self::$catId,
            'shop' => self::$shopId,
            'generalSpecs' => [
                'specsClass' => 'PhoneTablet',
                'source' => 'https://www.google.tn',
                'videoSite' => 'youtube',
                'video' => 'videoId11Xd',
                'description' => 'description colors',
                'colors' => 'KJML58,LKJ87H,lKJ879',
                'one' => 'Android 5.1',
                'two' => '3Go',
                'three' => '5.2',
                'four' => '1920',
                'five' => '1080',
                'six' => 'value',
                'seven' => 'value',
                'eight' => 'value',
                'nine' => 'value',
                'ten' => 'value'
            ],
            'anyParent' => false,
            'parent' => NULL,
        ];

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( $this->uriPrefix, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchCategoryProduct() {

        $data = [
            'product' => [
                'name' => 'Product Celio Patched',
                'mark' => 'Celio Patched',
                'public' => true,
            ],
            'generalSpecs' => [
                'source' => 'https://www.bing.com',
                'description' => 'description patched',
                'two' => '30Go',
            ],
        ];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . '/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutCategoryProduct() {

        $data = [
            'product' => [
                'reference' => 'prod-4578-edited',
                'name' => 'Product Celio Edited',
                'mark' => 'Celio Edited',
                'public' => true,
            ],
            'category' => self::$catId,
            'generalSpecs' => [
                'specsClass' => 'PhoneTablet',
                'source' => 'https://www.google.tn',
                'videoSite' => 'youtube',
                'video' => 'videoId11Xd',
                'description' => 'description colors',
                'colors' => 'KJML58,LKJ87H,lKJ879',
                'one' => 'Android 5.1',
                'two' => '300Go',
                'three' => '5.2',
                'four' => '1920',
                'five' => '1080',
                'six' => 'value',
                'seven' => 'value',
                'eight' => 'value',
                'nine' => 'value',
                'ten' => 'value'
            ]
        ];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . '/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveCategoryProduct() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . '/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        $baseUri = getenv( 'TEST_BASE_URI' );
        self::$staticClient = new Client( [
            'base_uri' => $baseUri,
            'defaults' => [
                'exceptions' => false,
            ],
        ] );
        parent::bootKernel();


        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $images = $em->getRepository('AppBundle:ImageProduct')->findAll();
        foreach ( $images as $image ) {
            $em->remove($image);
        }
        $products = $em->getRepository('AppBundle:CategoryProduct')->findAll();
        foreach ( $products as $product ) {
            $em->remove($product);
        }
        $em->flush();

        parent::purgeDatabase();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_SUPER_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $shop = new Shop();
        $shop->setName( 'Celio' );
        $shop->setDescription( 'Description of the celio shop...' );
        $shop->setClientNb( 871547 );
        $shop->setOfferNb( 658 );
        $shop->setWebsite( 'http://www.celio.fr' );
        $shop->setSlogan( 'Celio Slogan!' );
        $em->persist( $shop );

        $category = new Category();
        $category->setName( "Phone & Tablets" );
        $category->setLevel( 1 );
        $category->setSpecsClass( "PhoneTablet" );
        $em->persist( $category );

        $product = new Product();
        $product->setName( "My first Prod" );
        $product->setMark( "Exist" );
        $product->setReference( "prod-5879" );
        $em->persist( $product );

        $image = new Image();
        $image->setEntity( "Product" );
        $image->setImage( "gsjkfvqmfnjqsmv.png" );
        $image->setType( "cover" );
        $em->persist( $image );

        $generalspec = new GeneralSpec();
        $generalspec->setSpecsClass( "PhoneTablet" );
        $generalspec->setDescription( "description" );
        $generalspec->setColors( "KJOI58,KH87LO,YT87LO" );
        $generalspec->setSource( "http://www.google.com" );
        $generalspec->setVideo( "videocode" );
        $generalspec->setVideoSite( "youtube" );
        $generalspec->setOne( "one" );
        $generalspec->setTwo( "two" );
        $generalspec->setThree( "three" );
        $generalspec->setFour( "four" );
        $generalspec->setFive( "five" );
        $em->persist( $generalspec );

        $catProd = new CategoryProduct();
        $catProd->setGeneralSpecs( $generalspec );
        $catProd->setProduct( $product );
        $catProd->setCategory( $category );
        $catProd->setImage( $image );
        $em->persist( $catProd );

        $imgProd = new ImageProduct( $catProd, $image );
        $em->persist( $imgProd );

        $em->flush();

        /**
         * @var CategoryProduct $categoryProduct
         */
        $categoryProduct = $em->getRepository( 'AppBundle:CategoryProduct' )->getCatProductByName('My first Prod');
        $category = $em->getRepository( 'AppBundle:Category' )->findOneBy(['specsClass'=>'PhoneTablet']);
        $shop = $em->getRepository( 'AppBundle:Shop' )->findOneBy( [ 'name' => 'Celio' ] );
        self::$id = $categoryProduct->getId();
        self::$slug = $categoryProduct->getProduct()->getSlug();
        self::$shopId = $shop->getId();
        self::$catId = $category->getId();
    }
}
