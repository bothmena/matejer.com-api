<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryProduct;
use AppBundle\Entity\Collection;
use AppBundle\Entity\CollectionProduct;
use AppBundle\Entity\GeneralSpec;
use AppBundle\Entity\Image;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductShop;
use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class CollectionControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/shops/';
    private static $shopId;
    private static $shopSlug;
    private static $catSlug;
    private static $id;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [ 'id', 'category', 'parent', 'shop', 'name', 'slug', 'product_nb', 'level',
        'any_parent', 'date' ];

    public function testGetCollection() {

        $response = $this->client->get( $this->uriPrefix . self::$shopId . '/collections/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetCollectionsByInsSlug() {

        $response = $this->client->get( $this->uriPrefix . self::$shopSlug . '/collections' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testGetCollections() {

        $response = $this->client->get( $this->uriPrefix . self::$shopId . '/collections' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testGetCollectionProducts() {

        $response = $this->client->get( $this->uriPrefix . self::$shopId . '/collections/' .
            self::$id . '/products' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $keysArray = ['collection', 'products'];
        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $keysArray ), sizeof( $contentArray ) );
    }

    public function testGetCollectionProductsByShopSlug() {

        $response = $this->client->get( $this->uriPrefix . self::$shopSlug . '/collections/' .
            self::$id . '/products' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $keysArray = ['collection', 'products'];
        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $keysArray ), sizeof( $contentArray ) );
    }

    public function testNewCollection() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $collectionData = [
            'name' => 'Collection Name',
            'level' => '2',
            'category' => self::$catSlug,
            'anyParent' => false,
            'parent' => NULL,
        ];

        $response = $this->client->post( $this->uriPrefix . self::$shopId . '/collections', [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $collectionData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchCollection() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $collectionData = [
            'name' => 'Collection Name Edited',
            'level' => '1',
        ];

        $response = $this->client->patch( $this->uriPrefix . self::$shopId . '/collections/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $collectionData ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutCollection() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $collectionData = [
            'name' => 'Collection Name Changed',
            'level' => '3',
            'category' => self::$catSlug,
            'anyParent' => false,
            'parent' => NULL,
        ];

        $response = $this->client->put( $this->uriPrefix . self::$shopId . '/collections/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $collectionData ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveCollection() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$shopId . '/collections/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $shop = new Shop();
        $shop->setName( 'Celio' );
        $shop->setDescription( 'Description of the celio shop...' );
        $shop->setClientNb( 871547 );
        $shop->setOfferNb( 658 );
        $shop->setWebsite( 'http://www.celio.fr' );
        $shop->setSlogan( 'Celio Slogan!' );
        $em->persist( $shop );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_SUPER_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $user->setMyShop( $shop );
        $em->persist( $user );

        $category = new Category();
        $category->setName( "Phone & Tablets" );
        $category->setLevel( 1 );
        $category->setSpecsClass( "PhoneTablet" );
        $em->persist( $category );

        $product = new Product();
        $product->setName( "My first Prod" );
        $product->setMark( "Exist" );
        $product->setReference( "prod-5879" );
        $em->persist( $product );

        $image = new Image();
        $image->setEntity( "Product" );
        $image->setImage( "gsjkfvqmfnjqsmv.png" );
        $image->setType( "cover" );
        $em->persist( $image );

        $generalspec = new GeneralSpec();
        $generalspec->setSpecsClass( "PhoneTablet" );
        $generalspec->setDescription( "description" );
        $generalspec->setColors( "KJOI58,KH87LO,YT87LO" );
        $generalspec->setSource( "http://www.google.com" );
        $generalspec->setVideo( "videocode" );
        $generalspec->setVideoSite( "youtube" );
        $generalspec->setOne( "one" );
        $generalspec->setTwo( "two" );
        $generalspec->setThree( "three" );
        $generalspec->setFour( "four" );
        $generalspec->setFive( "five" );
        $em->persist( $generalspec );

        $catProd = new CategoryProduct();
        $catProd->setGeneralSpecs( $generalspec );
        $catProd->setProduct( $product );
        $catProd->setCategory( $category );
        $catProd->setImage( $image );
        $em->persist( $catProd );

        $productShop1 = new ProductShop();
        $productShop1->setShop( $shop );
        $productShop1->setCategoryProduct( $catProd );
        $productShop1->setReference( 'ref-1992' );
        $productShop1->setAvailability( 'avail' );
        $productShop1->setWarranty( 36 );
        $productShop1->setPrice( 599.36 );
        $productShop1->setDiscount( 10 );
        $em->persist( $productShop1 );

        $productShop2 = new ProductShop();
        $productShop2->setShop( $shop );
        $productShop2->setCategoryProduct( $catProd );
        $productShop2->setReference( 'ref-1993' );
        $productShop2->setAvailability( 'demnd' );
        $productShop2->setWarranty( 24 );
        $productShop2->setPrice( 899.36 );
        $em->persist( $productShop2 );

        $collection = new Collection();
        $collection->setShop( $shop );
        $collection->setLevel( 1 );
        $collection->setName( 'Collection 1' );
        $collection->setCategory( $category );
        $em->persist( $collection );

        $collectionProd1 = new CollectionProduct($collection, $productShop1);
        $collectionProd2 = new CollectionProduct($collection, $productShop2);
        $em->persist( $collectionProd1 );
        $em->persist( $collectionProd2 );

        $em->flush();

        /**
         * @var $collection Collection
         */
        $collection = $em->getRepository( 'AppBundle:Collection' )->findOneBy( [ 'name' => 'Collection 1' ] );
        self::$id = $collection->getId();
        self::$shopId = $collection->getShop()->getId();
        self::$shopSlug = $collection->getShop()->getSlug();
        self::$catSlug = $collection->getCategory()->getId();
    }
}
