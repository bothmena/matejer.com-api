<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Feature;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use GuzzleHttp\Client;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class FeatureControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/products/';
    private static $productId;
    private static $productSlug;
    private static $id;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = ['category', 'image', 'name', 'description', 'locale'];

    public function testGetFeature() {

        $response = $this->client->get( $this->uriPrefix . self::$productId . '/features/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetFeatures() {

        $response = $this->client->get( $this->uriPrefix . self::$productId . '/features' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewFeature() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $featureData = [
            'category' => '5',
            'image' => '5',
            'name' => 'Feature name',
            'description' => 'feature description that should be long enough........',
            'locale' => 'en'
        ];

        $response = $this->client->post( $this->uriPrefix . self::$productId . '/features', [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $featureData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchFeature() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . self::$productId . '/features/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( ['category' => '5', 'image' => '5', 'name' => 'Feature name', 'description' => 'feature description that should be long enough........', 'locale' => 'en'] ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutFeature() {

        $data = ['category' => '5', 'image' => '5', 'name' => 'Feature name', 'description' => 'feature description that should be long enough........', 'locale' => 'en'];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . self::$productId . '/features/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveFeature() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$productId . '/features/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ]
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $product = new Product();
        // Product Setters

        $em->persist( $product );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_USER' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $feature = new Feature();
        $feature->setProduct( $product );
        // Feature setters
        $em->persist( $feature );

        $em->flush();

        /**
         * @var $feature Feature
         */
        $feature = $em->getRepository( 'AppBundle:Feature' )->findOneBy( [ 'field' => 'value' ] );
        self::$id = $feature->getId();
        self::$productId = $feature->getProduct()->getId();
        self::$productSlug = $feature->getProduct()->getSlug();
    }
}
