<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Article;
use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryProduct;
use AppBundle\Entity\GeneralSpec;
use AppBundle\Entity\Image;
use AppBundle\Entity\ImageProduct;
use AppBundle\Entity\Institute;
use AppBundle\Entity\Paragraph;
use AppBundle\Entity\Product;
use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use GuzzleHttp\Client;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

/**
 * Created by PhpStorm.
 * User: Aymen Ben Othmen
 * Date: 08/09/16
 * Time: 08:15
 */
class ImageControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/images';
    private static $id;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private static $userId;
    private static $shopId;
    private static $productId;
    private $imageArray = [
        'id',
        'image',
        'entity',
        'type',
        'date',
    ];

    public function testGetImage() {

        $response = $this->client->get("$this->uriPrefix/" . self::$id);

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue($response->hasHeader('Content-Type'));
        $this->assertEquals( $response->getHeader('Content-Type')[0], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->imageArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetImages() {

        $response = $this->client->get($this->uriPrefix);

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue($response->hasHeader('Content-Type'));
        $this->assertEquals( $response->getHeader('Content-Type')[0], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->imageArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[0] );
        }
    }

    public function testNewImage() {

        $path = '/home/bothmena/Pictures/screenshot.png';
        $entities = [ 'product' => self::$productId ];

        foreach ( $entities as $entity => $id ) {

            $url = $this->uriPrefix . '/' . $entity . '/' . $id . '/profile';
            $response = $this->client->request('POST', $url, [
                'multipart' => [
                    /*[
                        'name'     => 'foo',
                        'contents' => 'data',
                    ],*/
                    [
                        'name'     => 'app_image[file]',
                        'contents' => fopen($path, 'r'),
                        'filename' => 'screenshot.png'
                    ],
                ]
            ]);

            $this->assertEquals( 201, $response->getStatusCode() );
            $contentArray = json_decode( $response->getBody(), true );
            foreach ( $this->imageArray as $key ) {
                $this->assertArrayHasKey( $key, $contentArray );
            }
        }
    }

    public function testEditPatchImage() {

        $data = array(
            'entity' => 'usr',
        );

        $response = $this->client->patch("$this->uriPrefix/" . self::$id, [
            'body'=>json_encode($data)
        ]);
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testEditPutImage() {

        $data = array(
            'entity' => 'usr',
            'type' => 'cvr',
        );

        $response = $this->client->put("$this->uriPrefix/" . self::$id, [
            'body'=>json_encode($data)
        ]);
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testRemoveImage() {

        $response = $this->client->delete( "$this->uriPrefix/" . self::$id );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        $baseUri = getenv( 'TEST_BASE_URI' );
        self::$staticClient = new Client( [
            'base_uri' => $baseUri,
            'defaults' => [
                'exceptions' => false,
            ],
        ] );
        parent::bootKernel();


        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $images = $em->getRepository('AppBundle:ImageProduct')->findAll();
        foreach ( $images as $image ) {
            $em->remove($image);
        }
        $products = $em->getRepository('AppBundle:CategoryProduct')->findAll();
        foreach ( $products as $product ) {
            $em->remove($product);
        }
        $em->flush();

        parent::purgeDatabase();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService('doctrine.orm.entity_manager');
        $userManager = self::getService( 'fos_user.user_manager' );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_SUPER_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $shop = new Shop();
        $shop->setName( 'Celio' );
        $shop->setDescription( 'Description of the celio shop...' );
        $shop->setClientNb( 871547 );
        $shop->setOfferNb( 658 );
        $shop->setWebsite( 'http://www.celio.fr' );
        $shop->setSlogan( 'Celio Slogan!' );
        $em->persist( $shop );

        $category = new Category();
        $category->setName( "Phone & Tablets" );
        $category->setLevel( 1 );
        $category->setSpecsClass( "PhoneTablet" );
        $em->persist( $category );

        $product = new Product();
        $product->setName( "My first Prod" );
        $product->setMark( "Exist" );
        $product->setReference( "prod-5879" );
        $em->persist( $product );


        $name = 'A713RRJfX4nJSia5609O1t2jAkaqrLsfKFGzTR7oQsc.png';
        $image = new Image();
        $image->setEntity( "prd" );
        $image->setImage( $name );
        $image->setType( "cvr" );
        $em->persist( $image );

        $generalSpecs = new GeneralSpec();
        $generalSpecs->setSpecsClass( "PhoneTablet" );
        $generalSpecs->setDescription( "description" );
        $generalSpecs->setColors( "KJOI58,KH87LO,YT87LO" );
        $generalSpecs->setSource( "http://www.google.com" );
        $generalSpecs->setVideo( "videocode" );
        $generalSpecs->setVideoSite( "youtube" );
        $generalSpecs->setOne( "one" );
        $generalSpecs->setTwo( "two" );
        $generalSpecs->setThree( "three" );
        $generalSpecs->setFour( "four" );
        $generalSpecs->setFive( "five" );
        $em->persist( $generalSpecs );

        $catProd = new CategoryProduct();
        $catProd->setGeneralSpecs( $generalSpecs );
        $catProd->setProduct( $product );
        $catProd->setCategory( $category );
        $catProd->setImage( $image );
        $em->persist( $catProd );

        $imgProd = new ImageProduct( $catProd, $image );
        $em->persist( $imgProd );

        $em->flush();

        $image = $em->getRepository( 'AppBundle:Image' )->findOneBy(['image'=>$name]);
        self::$id = $image->getId();

        $categoryProduct = $em->getRepository( 'AppBundle:CategoryProduct' )->getCatProductByName('My first Prod');
        self::$productId = $categoryProduct->getId();

        $shop = $em->getRepository( 'AppBundle:Shop' )->findOneBy( [ 'name' => 'Celio' ] );
        self::$shopId = $shop->getId();

        $user = $userManager->findUserByUsername(self::$username);
        self::$userId = $user->getId();
    }
}
