<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryProduct;
use AppBundle\Entity\GeneralSpec;
use AppBundle\Entity\Image;
use AppBundle\Entity\ImageProduct;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use GuzzleHttp\Client;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class GeneralSpecControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/products/';
    private static $productId;
    private static $productSlug;
    private static $id;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = ['specs_class', 'source', 'video_site', 'video', 'description', 'k_j_m_l58_l_k_j87_hl_k_j879', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];

    public function testGetGeneralSpec() {

        $response = $this->client->get( $this->uriPrefix . self::$productId . '/generalspecs/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testNewGeneralSpec() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $generalspecData = ['specsClass' => 'specClass', 'source' => 'source_url', 'videoSite' => 'youtube', 'video' => 'videoId11Xd', 'description' => 'colors', 'KJML58,LKJ87H,lKJ879' => 'colors', 'one' => 'Android 5.1', 'two' => '3Go', 'three' => '5.2', 'four' => '1920', 'five' => '1080', 'six' => 'value', 'seven' => 'value', 'eight' => 'value', 'nine' => 'value', 'ten' => 'value'];

        $response = $this->client->post( $this->uriPrefix . self::$productId . '/generalspecs', [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $generalspecData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchGeneralSpec() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . self::$productId . '/generalspecs/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( ['specsClass' => 'specClass', 'source' => 'source_url', 'videoSite' => 'youtube', 'video' => 'videoId11Xd', 'description' => 'colors', 'KJML58,LKJ87H,lKJ879' => 'colors', 'one' => 'Android 5.1', 'two' => '3Go', 'three' => '5.2', 'four' => '1920', 'five' => '1080', 'six' => 'value', 'seven' => 'value', 'eight' => 'value', 'nine' => 'value', 'ten' => 'value'] ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutGeneralSpec() {

        $data = [
            'specsClass' => 'PhoneTablet',
            'source' => 'https://www.google.tn',
            'videoSite' => 'youtube',
            'video' => 'videoId11Xd',
            'description' => 'colors',
            'KJML58,LKJ87H,lKJ879' => 'colors',
            'one' => 'Android 5.1',
            'two' => '3Go',
            'three' => '5.2',
            'four' => '1920',
            'five' => '1080',
            'six' => 'value',
            'seven' => 'value',
            'eight' => 'value',
            'nine' => 'value',
            'ten' => 'value'
        ];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . self::$productId . '/generalspecs/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveGeneralSpec() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$productId . '/generalspecs/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ]
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_USER' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $category = new Category();
        $category->setName("Phone & Tablets");
        $category->setLevel(1);
        $category->setSpecsClass("PhoneTablet");
        $em->persist( $category );

        $product = new Product();
        $product->setName( "My first Prod" );
        $product->setMark( "Exist" );
        $product->setReference( "prod-5879" );
        $em->persist( $product );

        $image = new Image();
        $image->setEntity( "Product" );
        $image->setImage( "gsjkfvqmfnjqsmv.png" );
        $image->setType( "cover" );
        $em->persist( $image );

        $generalspec = new GeneralSpec();
        $generalspec->setSpecsClass("PhoneTablet");
        $generalspec->setDescription("description");
        $generalspec->setColors("KJOI58,KH87LO,YT87LO");
        $generalspec->setSource("http://www.google.com");
        $generalspec->setVideo("videocode");
        $generalspec->setVideoSite("youtube");
        $generalspec->setOne("one");
        $generalspec->setTwo("two");
        $generalspec->setThree("three");
        $generalspec->setFour("four");
        $generalspec->setFive("five");
        $em->persist( $generalspec );

        $catProd = new CategoryProduct();
        $catProd->setGeneralSpecs( $generalspec );
        $catProd->setProduct( $product );
        $catProd->setCategory( $category );
        $catProd->setImage( $image );
        $em->persist( $catProd );

        $imgProd = new ImageProduct( $catProd, $image );
        $em->persist( $imgProd );

        $em->flush();

        /**
         * @var $generalspec GeneralSpec
         */
        $generalspec = $em->getRepository( 'AppBundle:GeneralSpec' )->findOneBy( [ 'one' => 'one' ] );
        $product = $em->getRepository( 'AppBundle:Product' )->findOneBy( [ 'mark' => 'Exist' ] );
        self::$id = $generalspec->getId();
        self::$productId = $product->getId();
        self::$productSlug = $product->getSlug();
    }
}
