<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\FicheProduct;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use GuzzleHttp\Client;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class FicheProductControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/products/';
    private static $productId;
    private static $productSlug;
    private static $id;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = ['category_procuct', 'name', 'group', 'value', 'type', 'language'];

    public function testGetFicheProduct() {

        $response = $this->client->get( $this->uriPrefix . self::$productId . '/ficheproducts/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetFicheProducts() {

        $response = $this->client->get( $this->uriPrefix . self::$productId . '/ficheproducts' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewFicheProduct() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $ficheproductData = [
            'categoryProcuct' => '5',
            'name' => 'RAM',
            'group' => 'Hardware',
            'value' => '32 Go',
            'type' => 'double|simple',
            'language' => 'en'
        ];

        $response = $this->client->post( $this->uriPrefix . self::$productId . '/ficheproducts', [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $ficheproductData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchFicheProduct() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . self::$productId . '/ficheproducts/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( ['categoryProcuct' => '5', 'name' => 'RAM', 'group' => 'Hardware', 'value' => '32 Go', 'type' => 'double|simple', 'language' => 'en'] ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutFicheProduct() {

        $data = ['categoryProcuct' => '5', 'name' => 'RAM', 'group' => 'Hardware', 'value' => '32 Go', 'type' => 'double|simple', 'language' => 'en'];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . self::$productId . '/ficheproducts/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveFicheProduct() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$productId . '/ficheproducts/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ]
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $product = new Product();
        // Product Setters

        $em->persist( $product );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_USER' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $ficheproduct = new FicheProduct();
        $ficheproduct->setProduct( $product );
        // FicheProduct setters
        $em->persist( $ficheproduct );

        $em->flush();

        /**
         * @var $ficheproduct FicheProduct
         */
        $ficheproduct = $em->getRepository( 'AppBundle:FicheProduct' )->findOneBy( [ 'field' => 'value' ] );
        self::$id = $ficheproduct->getId();
        self::$productId = $ficheproduct->getProduct()->getId();
        self::$productSlug = $ficheproduct->getProduct()->getSlug();
    }
}
