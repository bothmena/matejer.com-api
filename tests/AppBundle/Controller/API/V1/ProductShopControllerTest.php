<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryProduct;
use AppBundle\Entity\Collection;
use AppBundle\Entity\CollectionProduct;
use AppBundle\Entity\GeneralSpec;
use AppBundle\Entity\Image;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductShop;
use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class ProductShopControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/shops/';
    private static $shopId;
    private static $shopSlug;
    private static $id;
    private static $col1;
    private static $col2;
    private static $col3;
    private static $prodId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = ['id', 'shop', 'category_product', 'reference', 'price', 'discount', 'warranty', 'availability', 'date'];

    public function testGetProductShop() {

        $response = $this->client->get( $this->uriPrefix . self::$shopId . '/products/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetProductShops() {

        $response = $this->client->get( $this->uriPrefix . self::$shopId . '/products' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testGetProductShopsBySlug() {

        $response = $this->client->get( $this->uriPrefix . self::$shopSlug . '/products' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewProductShop() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $productShopData = [
            'reference' => 'prod-shop-547',
            'price' => '5478',
            'discount' => '10',
            'warranty' => '36',
            'availability' => 'stock', // 'stock|comnd|avasn|unava'
            'categoryProduct' => self::$prodId,
            'collection' => [self::$col3, self::$col2],
        ];

        $response = $this->client->post( $this->uriPrefix . self::$shopId . '/products', [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $productShopData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchProductShop() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $productShopData = [
            'price' => '578',
            'discount' => '25',
            'warranty' => '24',
            'availability' => 'avasn', // 'stock|comnd|avasn|unava'
            'collection' => [self::$col3, self::$col2],
        ];

        $response = $this->client->patch( $this->uriPrefix . self::$shopId . '/products/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $productShopData ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutProductShop() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $data = [
            'reference' => 'prod-shop-547',
            'price' => '12',
            'discount' => '0',
            'warranty' => '60',
            'availability' => 'comnd',
            'categoryProduct' => self::$prodId,
            'collection' => [self::$col3],
        ];

        $response = $this->client->put( $this->uriPrefix . self::$shopId . '/products/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveProductShop() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$shopId . '/products/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ]
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $shop = new Shop();
        $shop->setName( 'Celio' );
        $shop->setDescription( 'Description of the celio shop...' );
        $shop->setClientNb( 871547 );
        $shop->setOfferNb( 658 );
        $shop->setWebsite( 'http://www.celio.fr' );
        $shop->setSlogan( 'Celio Slogan!' );
        $em->persist( $shop );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_SUPER_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $user->setMyShop( $shop );
        $em->persist( $user );

        $category = new Category();
        $category->setName( "Phone & Tablets" );
        $category->setLevel( 1 );
        $category->setSpecsClass( "PhoneTablet" );
        $em->persist( $category );

        $product = new Product();
        $product->setName( "My first Prod" );
        $product->setMark( "Exist" );
        $product->setReference( "prod-5879" );
        $em->persist( $product );

        $image = new Image();
        $image->setEntity( "Product" );
        $image->setImage( "gsjkfvqmfnjqsmv.png" );
        $image->setType( "cover" );
        $em->persist( $image );

        $generalSpec = new GeneralSpec();
        $generalSpec->setSpecsClass( "PhoneTablet" );
        $generalSpec->setDescription( "description" );
        $generalSpec->setColors( "KJOI58,KH87LO,YT87LO" );
        $generalSpec->setSource( "http://www.google.com" );
        $generalSpec->setVideo( "videocode" );
        $generalSpec->setVideoSite( "youtube" );
        $generalSpec->setOne( "one" );
        $generalSpec->setTwo( "two" );
        $generalSpec->setThree( "three" );
        $generalSpec->setFour( "four" );
        $generalSpec->setFive( "five" );
        $em->persist( $generalSpec );

        $catProd = new CategoryProduct();
        $catProd->setGeneralSpecs( $generalSpec );
        $catProd->setProduct( $product );
        $catProd->setCategory( $category );
        $catProd->setImage( $image );
        $em->persist( $catProd );

        $productShop = new ProductShop();
        $productShop->setShop( $shop );
        $productShop->setCategoryProduct( $catProd );
        $productShop->setReference( 'ref-1992' );
        $productShop->setAvailability( 'avail' );
        $productShop->setWarranty( 36 );
        $productShop->setPrice( 599.36 );
        $productShop->setDiscount( 10 );
        $em->persist( $productShop );

        $collection1 = new Collection();
        $collection1->setShop( $shop );
        $collection1->setLevel( 1 );
        $collection1->setProductNb( 1 );
        $collection1->setName( 'Collection 1' );
        $collection1->setCategory( $category );
        $em->persist( $collection1 );

        $collection2 = new Collection();
        $collection2->setShop( $shop );
        $collection2->setLevel( 1 );
        $collection2->setProductNb( 1 );
        $collection2->setName( 'Collection 2' );
        $collection2->setCategory( $category );
        $em->persist( $collection2 );

        $collection3 = new Collection();
        $collection3->setShop( $shop );
        $collection3->setLevel( 1 );
        $collection3->setName( 'Collection 3' );
        $collection3->setCategory( $category );
        $em->persist( $collection3 );

        $collectionProd1 = new CollectionProduct( $collection1, $productShop );
        $em->persist( $collectionProd1 );
        $collectionProd2 = new CollectionProduct( $collection2, $productShop );
        $em->persist( $collectionProd2 );

        $em->flush();

        /**
         * @var $productshop ProductShop
         */
        $productshop = $em->getRepository( 'AppBundle:ProductShop' )->findOneBy( [ 'reference' => 'ref-1992' ] );
        self::$col1 = $em->getRepository( 'AppBundle:Collection' )->findOneBy( [ 'name' => 'Collection 1' ] )->getId();
        self::$col2 = $em->getRepository( 'AppBundle:Collection' )->findOneBy( [ 'name' => 'Collection 2' ] )->getId();
        self::$col3 = $em->getRepository( 'AppBundle:Collection' )->findOneBy( [ 'name' => 'Collection 3' ] )->getId();
        self::$id = $productshop->getId();
        self::$shopId = $productshop->getShop()->getId();
        self::$prodId = $productshop->getCategoryProduct()->getId();
        self::$shopSlug = $productshop->getShop()->getSlug();
    }
}
