<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class ShopControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/shops';
    private static $id;
    private static $slug;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        'id',
        'location',
        'rate_stat',
        'image',
        'cover',
        'name',
        'slug',
        'currency',
        'inscription_date',
        'description',
        'website',
        'slogan',
        'client_nb',
        'offer_nb',
    ];

    public function testGetShop() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetShopBySlug() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$slug );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetShops() {

        $response = $this->client->get( $this->uriPrefix );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewShop() {

        $data = [
            'location' => [
                'country' => 'TN',
                'state' => 'Ariana',
                'city' => 'Raoued',
                'address' => '24 Rue Freedom',
                'postalCode' => '8547',
            ],
            'name' => 'Shop',
            'description' => 'the description of the Shop boutiques...',
            'website' => 'http://www.matejer.com/shop',
            'slogan' => 'The Shop Slogan'
        ];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( $this->uriPrefix, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchShop() {

        $data = [
            'name' => 'Patched Shop',
            'slogan' => 'The Patched Shop Slogan'
        ];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . '/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutShop() {

        $data = [
            'location' => [
                'country' => 'TN',
                'state' => 'Ariana',
                'city' => 'Raoued',
                'address' => '24 Rue Freedom',
                'postalCode' => '8547',
            ],
            'name' => 'Edited Shop',
            'description' => 'the description of the Shop boutiques...',
            'website' => 'http://www.matejer.com/shop',
            'slogan' => 'The Shop Slogan'
        ];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . '/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveShop() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . '/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_SUPER_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $shop = new Shop();
        $shop->setName( 'Celio' );
        $shop->setDescription( 'Description of the celio shop...' );
        $shop->setClientNb( 871547 );
        $shop->setOfferNb( 658 );
        $shop->setWebsite( 'http://www.celio.fr' );
        $shop->setSlogan( 'Celio Slogan!' );

        $em->persist( $shop );
        $em->flush();

        $shop = $em->getRepository( 'AppBundle:Shop' )->findOneBy( [ 'name' => 'Celio' ] );
        self::$id = $shop->getId();
        self::$slug = $shop->getSlug();
    }
}
