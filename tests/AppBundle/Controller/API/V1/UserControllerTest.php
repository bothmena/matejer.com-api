<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Location;
use AppBundle\Entity\User;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class UserControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/users';
    private static $username = 'test_user';
    private static $email = 'email@islamm.com';
    private static $password = 'mySEC92pass';
    private static $token;
    private static $id;

    private $keysArray = [
        'image_url',
        'location_id',
        'birthday',
        'role',
        'shop_id',
        'id',
        'email',
        'username',
        'first_name',
        'last_name',
        'gender',
        'language',
        'facebook_id',
        'facebook_access_token',
        'google_id',
        'google_access_token',
    ];

    public function testGetUser() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        $this->assertEquals( sizeof( $contentArray ), sizeof( $this->keysArray ) );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetByUsernameUser() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$username );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testUserExistByToken() {

        $response = $this->client->get( "$this->uriPrefix/t/" . self::$token . '?enabled=false' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );
        $this->assertArrayHasKey( 'exist', json_decode( $response->getBody(), true ) );
    }

    public function testGetUsers() {

        $response = $this->client->get( $this->uriPrefix );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewUser() {

        $data = [
            'username'      => 'username',
            'email'         => 'amail@gmail.com',
            'firstName'     => 'Aymen',
            'lastName'      => 'Ben Othmen',
            'birthday'      => '15/06/1992',
            'plainPassword' => [
                'password'         => 'myCORRECT6548PASS',
                'confirm_password' => 'myCORRECT6548PASS',
            ],
        ];

        $response = $this->client->post( $this->uriPrefix, [
            'body' => json_encode( $data ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testConfirmEmailUser() {

        $response = $this->client->get( "$this->uriPrefix/confirm-email/" . self::$token );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertContains( 'token', $response->getBody()->getContents() );
    }

    public function testResendCodeUser() {

        $response = $this->client->post( "$this->uriPrefix/resend-code/" . self::$email );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPatchUser() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );

        $authData = json_decode( $response->getBody(), true );

        $data = [
            'birthday' => '02/06/1992',
        ];

        $response = $this->client->patch( "$this->uriPrefix/" . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutUser() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $data = [
            'username'      => 'username',
            'email'         => 'amail@gmail.com',
            'firstName'     => 'Aymennnn',
            'lastName'      => 'Ben Othmen',
            'birthday'      => '15/06/1992',
            'gender'    => 'female',
            'language'  => 'ar',
        ];

        $response = $this->client->put( "$this->uriPrefix/" . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testResetPasswordUser() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $newPass = 'MyNew5ecPa55_#';

        $response = $this->client->post( "$this->uriPrefix/" . self::$id . '/reset-password', [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( [
                'oldPassword'   => self::$password,
                'plainPassword' => [
                    'password'         => $newPass,
                    'confirm_password' => $newPass,
                ],
            ] ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => 'MyNew5ecPa55_#',
            ],
        ] );
        $this->assertEquals( 200, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $address = new Location();
        $address->setCountry( 'tn' );
        $address->setState( 'Nabeul' );
        $address->setCity( 'Nabeul' );
        $address->setAddress( 'my address in some place!' );
        $address->setPostalCode( '8000' );

        $user = $userManager->createUser();
        $user->setEmail( self::$email );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setBirthday( new \DateTime( '1992-02-15' ) );
        $user->setLanguage( 'ar' );
        $user->setGender( 'male' );
        $user->setLocation( $address );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
//        self::$token = self::getService( 'fos_user.util.token_generator' )->generateToken();
//        $user->setConfirmationToken( self::$token );

        $em->persist( $user );
        $em->flush();

        $user = $userManager->findUserByEmail( self::$email );
        self::$id = $user->getId();
    }
}
