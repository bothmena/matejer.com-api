<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 05/02/17
 * Time: 13:15
 */

namespace Tests\AppBundle\Controller\Api;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use GuzzleHttp\Client;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Output\ConsoleOutput;

abstract class APIGuzzleTestCase extends KernelTestCase implements ABOTestFixture {

    /**
     * @var ConsoleOutput
     */
    protected $output;
    /**
     * @var Client
     */
    protected $client;
    protected $webClient;
    protected static $staticClient;

    public static function setUpBeforeClass() {

        $baseUri = getenv( 'TEST_BASE_URI' );
        self::$staticClient = new Client( [
            'base_uri' => $baseUri,
            'defaults' => [
                'exceptions' => false,
            ],
        ] );
        self::bootKernel();
        self::purgeDatabase();
    }

    protected static function createWebClient( array $options = [], array $server = [] ) {

        static::bootKernel( $options );

        $client = static::$kernel->getContainer()->get( 'test.client' );
        $client->setServerParameters( $server );

        return $client;
    }

    protected function setUp() {

        $this->client = self::$staticClient;
        $this->output = new ConsoleOutput();
    }

    /**
     * Clean up Kernel usage in this test.
     */
    protected function tearDown() {
        // purposefully not calling parent class, which shuts down the kernel
    }

    protected static function purgeDatabase() {

        $purger = new ORMPurger( self::getService( 'doctrine.orm.default_entity_manager' ) );
        $purger->purge();
    }

    protected static function getService( $id ) {

        return self::$kernel->getContainer()
            ->get( $id );
    }

    protected function onNotSuccessfulTest( \Throwable $throwable ) {

        if ( get_class( $throwable ) === 'PHPUnit_Framework_ExpectationFailedException' )
            $this->output->writeln( "<error>Assert Exception:</error> $throwable" );
        else {
            try {
                $this->debugResponse( $throwable->getResponse(), $throwable->getRequest() );
            }
            catch ( \Throwable $t ) {
                $this->output->writeln( '<error>Exception has no method called getResponse.</error>' );
                $this->output->writeln( '<info>Exception class: </info>' . get_class( $throwable ) );
                $this->output->writeln( '<info>Exception: </info>' . $throwable );
            }
        }
    }

    protected function debugResponse( ResponseInterface $response, RequestInterface $request ) {

        try {
            $this->output->writeln( '<info>Request Debug: </info>' );
            $this->output->writeln( 'Uri: ' . $request->getUri() );
            $this->output->writeln( 'Method: ' . $request->getMethod() );
            $this->output->writeln( 'Protocol Version: ' . $request->getProtocolVersion() );
            $this->output->writeln( 'Headers: ' );
            foreach ( $request->getHeaders() as $name => $val )
                $this->output->writeln( "\t$name: " . $request->getHeaderLine( $name ) );
            $this->output->writeln( 'Body: ' . $request->getBody() );
        } catch ( \Throwable $throwable ) {
            $this->output->writeln( '<error>An Error occured while debugging the request.</error>' );
            $this->output->writeln( "<error>$throwable</error>" );
        } finally {
            $this->output->writeln( '' );
        }

        try {
            $this->output->writeln( '<info>Response Debug: </info>' );
            $this->output->writeln( '<info>Status Code: </info> ' . $response->getStatusCode() );
            $content = (string) $response->getBody();
            $data = json_decode( $content, true );
            if ( isset( $data[ 'error_code' ] ) ) {
                $this->output->writeln( "<info>Error Code: </info>" . $data[ 'error_code' ] );
            }
            if ( isset( $data[ 'message' ] ) ) {
                $this->output->writeln( "<info>Message: </info>" . $data[ 'message' ] );
            }
            if ( $data === NULL ) {

                $this->output->writeln( '<error>Invalid Json: </error>' . $content );
            } else if ( isset( $data[ 'error' ] ) && is_array( $data[ 'error' ] ) ) {

                if ( $data[ 'error' ][ 'code' ] && $data[ 'error' ][ 'message' ] ) {
                    $this->output->writeln( 'Code: ' . $data[ 'error' ][ 'code' ] );
                    $this->output->writeln( 'Message: ' . $data[ 'error' ][ 'message' ] );
                    $this->output->writeln( 'Exceptions:' );
                    if ( is_array( $data[ 'error' ][ 'exception' ] ) ) {
                        foreach ( $data[ 'error' ][ 'exception' ] as $key => $arr ) {

                            $this->output->writeln( "[$key] Class: " . $arr[ 'class' ] );
                            $this->output->writeln( "[$key] Message: " . $arr[ 'message' ] );
                            $this->output->writeln( '' );
                        }
                    }
                }
            } else if ( isset( $data[ 'errors' ] ) && is_array( $data[ 'errors' ] ) ) {

                $this->output->writeln( "Form Errors:" );
                foreach ( $data[ 'errors' ] as $key => $val ) {

                    if ( is_array( $val ) ) {
                        $jsonVal = json_encode($val);
                        $this->output->writeln( "\t[$key] => " . "<error>$jsonVal</error>" );
                    } else{
                        $this->output->writeln( "\t[$key] => " . "<error>$val</error>" );
                    }
                }
                $this->output->writeln( "Form Errors By Field:" );
                if ( isset($data[ 'errors' ][ 'children' ]) ) {
                    foreach ( $data[ 'errors' ][ 'children' ] as $field => $arr ) {

                        if ( empty( $arr ) )
                            $this->output->writeln( "\t$field: No Errors (array is empty)." );
                        else {
                            if ( isset( $arr[ 'errors' ] ) ) {
                                $this->output->writeln( "\t$field:" );
                                foreach ( $arr[ 'errors' ] as $key => $val )
                                    $this->output->writeln( "\t\t[$key] => $val" );
                            } else if ( isset( $arr[ 'children' ] ) ) {

                                foreach ( $arr[ 'children' ] as $field2 => $arr2 ) {

                                    if ( empty( $arr2[ 'errors' ] ) )
                                        $this->output->writeln( "\t\t$field2: No Errors (array is empty)." );
                                    else {
                                        $this->output->writeln( "\t\t$field2:" );
                                        foreach ( $arr2 as $key2 => $val2 )
                                            if ( is_array($val2) ) {
                                                $this->output->writeln( "\t\t\t[$key2] => " . json_encode($val2) );
                                            } else {
                                                $this->output->writeln( "\t\t\t[$key2] => $val2" );
                                            }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        } catch ( \Throwable $throwable ) {
            $this->output->writeln( '<error>An Error occured while debugging the response.</error>' );
            $this->output->writeln( "<error>$throwable</error>" );
        }
    }
}
