<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 08/02/17
 * Time: 19:49
 */

namespace AppBundle\EventSubscriber;

use AppBundle\AppEvents;
use AppBundle\Event\UserConfirmEmailSuccessEvent;
use AppBundle\Event\UserRegistrationSuccessEvent;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class SecurityEventSubscriber implements EventSubscriberInterface {

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;
    private $userManager;
    private $tokenGenerator;
    private $jwtManager;

    public function __construct( EntityManager $entityManager, UserManagerInterface $userManager, TokenGeneratorInterface $tokenGenerator,
                                 JWTManager $jwtManager ) {

        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->tokenGenerator = $tokenGenerator;
        $this->jwtManager = $jwtManager;
    }

    public static function getSubscribedEvents() {

        return [
            AppEvents::REGISTRATION_SUCCESS       => [
                [ 'onRegistrationSuccess' ],
            ],
            AppEvents::USER_CONFIRM_EMAIL_SUCCESS => [
                [ 'onUserConfirmEmailSuccess' ],
            ],
        ];
    }

    public function onRegistrationSuccess( UserRegistrationSuccessEvent $event ) {

        $user = $event->getUser();
        $user->setEnabled( false );
        $user->setConfirmationToken( $this->tokenGenerator->generateToken() );
//        $user->setProfileImage( $this->getProfileImage( $user->getGender(), $user->getRoles()[0] ) );

        $this->userManager->updateUser( $user );
        $this->entityManager->flush();
    }

    public function onUserConfirmEmailSuccess( UserConfirmEmailSuccessEvent $event ) {

        $user = $event->getUser();
        $user->setEnabled( true );
        $user->setConfirmationToken( NULL );
        $this->userManager->updateUser( $user );

        $token = $this->jwtManager->create( $user );
        $response = new JsonResponse( [ 'token' => $token ], 200 );
        $event->setResponse( $response );
    }
}
