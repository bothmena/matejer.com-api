<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 10/12/17
 * Time: 14:22
 */

namespace AppBundle\EventSubscriber;

use AppBundle\AppEvents;
use AppBundle\Entity\Image;
use AppBundle\Entity\ImageProduct;
use AppBundle\Entity\ImageUser;
use AppBundle\Entity\ImageShop;
use AppBundle\Event\ShopImageUploadSuccessEvent;
use AppBundle\Event\ProductImageUploadSuccessEvent;
use AppBundle\Event\UserImageUploadSuccessEvent;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;

class ImageUploadEventSubscriber implements EventSubscriberInterface {

    private $entityManage;
    private $serializer;

    public function __construct( EntityManager $manager, Serializer $serializer ) {

        $this->entityManage = $manager;
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents () {

        return array(
            AppEvents::SHOP_IMAGE_UPLOAD_SUCCESS => array(
                array('onShopImageUpload'),
            ),
            AppEvents::PRODUCT_IMAGE_UPLOAD_SUCCESS => array(
                array('onProductImageUpload'),
            ),
            AppEvents::USER_IMAGE_UPLOAD_SUCCESS      => array(
                array('onUserImageUpload'),
            ),
        );
    }

    public function onShopImageUpload( ShopImageUploadSuccessEvent $event ) {

        $shop = $event->getShop();
        $image = $event->getImage();

        if ( $image->getType() == 'profile' )
            $shop->setImage( $image );
        elseif ( $image->getType() == 'cover' )
            $shop->setCover( $image );

        $imageMosque = new ImageShop( $shop, $image );
        $this->entityManage->persist( $imageMosque );
        $this->entityManage->flush();

        $event->setResponse( $this->getResponse( $image ) );
    }

    public function onUserImageUpload( UserImageUploadSuccessEvent $event ) {

        $user = $event->getUser();
        $image = $event->getImage();
        echo "User email: " . $user->getEmail();

        if ( $image->getType() == 'profile' )
            $user->setImage( $image );

        $imageUser = new ImageUser($user, $image);
        $this->entityManage->persist( $imageUser );
        $this->entityManage->flush();

        $event->setResponse( $this->getResponse( $image ) );
    }

    public function onProductImageUpload( ProductImageUploadSuccessEvent $event ) {

        $categoryProduct = $event->getCategoryProduct();
        $image = $event->getImage();

        if ( $image->getType() == 'profile' )
            $categoryProduct->setImage( $image );

        $imageProduct = new ImageProduct( $categoryProduct, $image );
        $this->entityManage->persist( $imageProduct );
        $this->entityManage->flush();

        $event->setResponse( $this->getResponse( $image ) );
    }

    private function getResponse( Image $image ) {

        $json = $this->serializer->serialize( $image, 'json' );
        return new Response( $json, 201 );
    }
}
