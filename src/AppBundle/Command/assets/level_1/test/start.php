<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\User;
use AppBundle\Entity\Xxxxxx;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class XxxxxxControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/xxxxxxs';
    private static $id;
    private static $slug;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = ['FIELDS'];