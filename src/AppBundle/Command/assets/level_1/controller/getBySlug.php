

    /**
     * @Rest\Get(path="/xxxxxxs/{slug}", name="api_xxxxxx_read_by_slug", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single xxxxxx with all the fields unless fields parameter is provided.",
     *     section="Xxxxxx",
     *     requirements={
     *         {"name"="slug","dataType"="string","description"="Xxxxxx slug"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired xxxxxx fields, select from: [FIELDS], default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function getBySlugAction( Request $request, string $slug ) {

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->findOneBy( [ 'slug' => $slug ] );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.xxxxxx',
                'message'     => 'Xxxxxx was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $xxxxxx->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxx, 200 ) );
    }