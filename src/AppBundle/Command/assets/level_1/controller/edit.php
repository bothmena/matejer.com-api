

    /**
     * @Rest\Route(path="/xxxxxxs/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"},
     *     name="api_xxxxxx_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="Update xxxxxx details",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Xxxxxx id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being modified.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "edit", "patch-method", "put-method"},
     *     tags={"v1" = "#48A01D", "PUT" = "#381DA0", "PATCH" = "#1D85A0"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )->find( $id );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_xxxxxx',
                'message'     => 'Institute xxxxxx was not found',
            ], 404 );
        }

        return $this->processForm( $request, $xxxxxx, false );
    }