

    /**
     * @Rest\Get(path="/xxxxxxs", name="api_xxxxxx_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="Retrieve paginated list of xxxxxxs with all the fields unless fields parameter is provided.",
     *     parameters={
     *         {"name"="limit", "dataType"="integer", "required"=false, "description"="max xxxxxxs in response, default 20"},
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired xxxxxx fields, select from: [FIELDS], default: all"},
     *         {"name"="page", "dataType"="integer", "required"=false, "description"="the page number, default: 1"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "cget", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @return Response
     */
    public function cgetAction( Request $request ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $xxxxxxs = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->findAll();

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $xxxxxxs as $xxxxxx ) {

                $result[ $xxxxxx->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $xxxxxx->getId() ][ $field ] = $xxxxxx->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxxs, 200 ) );
    }