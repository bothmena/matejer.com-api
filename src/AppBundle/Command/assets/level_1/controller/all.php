<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\Xxxxxx;
use AppBundle\Form\XxxxxxType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class XxxxxxController extends FOSRestController {

    private $entityFields = ['FIELDS'];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/xxxxxxs/{id}", requirements={"id": "\d+"},
     *     name="api_xxxxxx_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single xxxxxx with all the fields unless fields parameter is provided.",
     *     section="Xxxxxx",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Xxxxxx id"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired xxxxxx fields, select from: all, [FIELDS], default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $id ) {

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->find( $id );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.xxxxxx',
                'message'     => 'Xxxxxx was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $xxxxxx->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxx, 200 ) );
    }

    /**
     * @Rest\Get(path="/xxxxxxs/{slug}", name="api_xxxxxx_read_by_slug", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single xxxxxx with all the fields unless fields parameter is provided.",
     *     section="Xxxxxx",
     *     requirements={
     *         {"name"="slug","dataType"="string","description"="Xxxxxx slug"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired xxxxxx fields, select from: all, [FIELDS], default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function getBySlugAction( Request $request, string $slug ) {

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->findOneBy( [ 'slug' => $slug ] );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.xxxxxx',
                'message'     => 'Xxxxxx was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $xxxxxx->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxx, 200 ) );
    }

    /**
     * @Rest\Get(path="/xxxxxxs", name="api_xxxxxx_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="Retrieve a paginated list of xxxxxxs with all the fields unless fields parameter is provided.",
     *     parameters={
     *         {"name"="limit", "dataType"="integer", "required"=false, "description"="max xxxxxxs in response, default 20"},
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired xxxxxx fields, select from: all, [FIELDS], default: all"},
     *         {"name"="page", "dataType"="integer", "required"=false, "description"="the page number, default: 1"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "get-list", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @return Response
     */
    public function cgetAction( Request $request ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $xxxxxxs = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->findAll();

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $xxxxxxs as $xxxxxx ) {

                $result[ $xxxxxx->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $xxxxxx->getId() ][ $field ] = $xxxxxx->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxxs, 200 ) );
    }

    /**
     * @Rest\Post(path="/xxxxxxs", name="api_xxxxxx_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="Create a new Xxxxxx Object",
     *     input="AppBundle\Form\XxxxxxType",
     *     output="AppBundle\Entity\Xxxxxx",
     *     statusCodes={
     *         201="Created: The request has been fulfilled and resulted in a new resource being created.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "new", "post-method"},
     *     tags={"v1" = "#48A01D", "POST" = "#1D3BA0"},
     * )
     * @param Request $request
     * @return Response
     */
    public function newAction( Request $request ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $xxxxxx = new Xxxxxx();

        return $this->processForm( $request, $xxxxxx, true );
    }

    /**
     * @Rest\Route(path="/xxxxxxs/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"},
     *     name="api_xxxxxx_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="Update xxxxxx details",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Xxxxxx id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being modified.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "edit", "patch-method", "put-method"},
     *     tags={"v1" = "#48A01D", "PUT" = "#381DA0", "PATCH" = "#1D85A0"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )->find( $id );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.xxxxxx',
                'message'     => 'Xxxxxx was not found',
            ], 404 );
        }

        return $this->processForm( $request, $xxxxxx, false );
    }

    /**
     * @Rest\Delete(path="/xxxxxxs/{id}", requirements={"id": "\d+"},
     *     name="api_xxxxxx_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="Delete a single xxxxxx",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Xxxxxx id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being deleted.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "delete", "delete-method"},
     *     tags={"v1" = "#48A01D", "DELETE" = "#A01D27"},
     * )
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )->find( $id );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.xxxxxx',
                'message'     => 'Xxxxxx was not found',
            ], 404 );
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove( $xxxxxx );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Xxxxxx $xxxxxx
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Xxxxxx $xxxxxx, bool $isNew = false ) {

        $form = $this->createForm( XxxxxxType::class, $xxxxxx );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $xxxxxx );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_xxxxxx_read', [
                    'version' => 'v1',
                    'id'      => $xxxxxx->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}
