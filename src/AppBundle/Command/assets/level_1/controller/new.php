

    /**
     * @Rest\Post(path="/xxxxxxs", name="api_xxxxxx_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="Create a new Xxxxxx Object",
     *     input="AppBundle\Form\XxxxxxType",
     *     output="AppBundle\Entity\Xxxxxx",
     *     statusCodes={
     *         201="Created: The request has been fulfilled and resulted in a new resource being created.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "new", "post-method"},
     *     tags={"v1" = "#48A01D", "POST" = "#1D3BA0"},
     * )
     * @param Request $request
     * @return Response
     */
    public function newAction( Request $request ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $xxxxxx = new Xxxxxx();

        return $this->processForm( $request, $xxxxxx, true );
    }