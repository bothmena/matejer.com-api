

    /**
     * @param Request $request
     * @param Xxxxxx $xxxxxx
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Xxxxxx $xxxxxx, bool $isNew = false ) {

        $form = $this->createForm( XxxxxxType::class, $xxxxxx );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $xxxxxx );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_xxxxxx_read', [
                    'version' => 'v1',
                    'id'      => $xxxxxx->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }