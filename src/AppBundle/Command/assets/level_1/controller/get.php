

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/xxxxxxs/{id}", requirements={"id": "\d+"},
     *     name="api_xxxxxx_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single xxxxxx with all the fields unless fields parameter is provided.",
     *     section="Xxxxxx",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Xxxxxx id"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired xxxxxx fields, select from: [FIELDS], default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $id ) {

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->find( $id );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.xxxxxx',
                'message'     => 'Xxxxxx was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $xxxxxx->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxx, 200 ) );
    }