

    /**
     * @Rest\Delete(path="/xxxxxxs/{id}", requirements={"id": "\d+"},
     *     name="api_xxxxxx_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="Delete a single xxxxxx",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Xxxxxx id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being deleted.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "xxxxxxs", "delete", "delete-method"},
     *     tags={"v1" = "#48A01D", "DELETE" = "#A01D27"},
     * )
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )->find( $id );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.xxxxxx',
                'message'     => 'Institute xxxxxx was not found',
            ], 404 );
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove( $xxxxxx );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }