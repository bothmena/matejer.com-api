

    public function testEditPatchXxxxxx() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . self::$ownerId . '/xxxxxxs/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( ['FIELDS'=>'VALUES'] ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutXxxxxx() {

        $data = ['FIELDS'=>'VALUES'];
        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . self::$ownerId . '/xxxxxxs/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }