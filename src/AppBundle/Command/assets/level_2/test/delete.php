

    public function testRemoveXxxxxx() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$ownerId . '/xxxxxxs/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ]
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }