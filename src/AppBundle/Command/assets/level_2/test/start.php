<?php

namespace Tests\AppBundle\Controller\Api\V1;

use AppBundle\Entity\Xxxxxx;
use AppBundle\Entity\Owner;
use AppBundle\Entity\User;
use GuzzleHttp\Client;
use Tests\AppBundle\Controller\Api\APIGuzzleTestCase;

class XxxxxxControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/app_test.php/api/v1/owners/';
    private static $ownerId;
    private static $ownerSlug;
    private static $id;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = ['FIELDS'];