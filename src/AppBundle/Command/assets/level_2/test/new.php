

    public function testNewXxxxxx() {

        $response = $this->client->post( '/app_test.php/api/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $xxxxxxData = ['FIELDS'=>'VALUES'];

        $response = $this->client->post( $this->uriPrefix . self::$ownerId . '/xxxxxxs', [
            'headers' => [
                'Authorization' => sprintf( 'Bearer %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $xxxxxxData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }