

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $owner = new Owner();
        // Owner Setters

        $em->persist( $owner );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setRoles( [ 'ROLE_USER' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $xxxxxx = new Xxxxxx();
        $xxxxxx->setOwner( $owner );
        // Xxxxxx setters
        $em->persist( $xxxxxx );

        $em->flush();

        /**
         * @var $xxxxxx Xxxxxx
         */
        $xxxxxx = $em->getRepository( 'AppBundle:Xxxxxx' )->findOneBy( [ 'field' => 'value' ] );
        self::$id = $xxxxxx->getId();
        self::$ownerId = $xxxxxx->getOwner()->getId();
        self::$ownerSlug = $xxxxxx->getOwner()->getSlug();
    }
}
