

    /**
     * @Rest\Route(path="/owners/{ownerId}/xxxxxxs/{id}", requirements={"ownerId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_xxxxxx_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Xxxxxxs"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $ownerId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $ownerId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->getOwnerXxxxxx( $ownerId, $id );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.owner_xxxxxx',
                'message'     => 'Owner xxxxxx was not found',
            ], 404 );
        }

        return $this->processForm( $request, $xxxxxx, false );
    }