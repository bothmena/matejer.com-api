

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/owners/{ownerId}/xxxxxxs/{id}", requirements={"ownerId": "\d+", "id": "\d+"},
     *     name="api_xxxxxx_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $ownerId
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $ownerId, int $id ) {

        $owner = $this->getDoctrine()->getRepository( 'AppBundle:Owner' )
            ->find( $ownerId );
        if ( !$owner ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.owner',
                'message'     => 'Owner was not found',
            ], 404 );
        }

        $xxxxxx = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->getOwnerXxxxxx( $ownerId, $id );
        if ( !$xxxxxx ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.owner_xxxxxx',
                'message'     => 'Owner xxxxxx was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $xxxxxx->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxx, 200 ) );
    }