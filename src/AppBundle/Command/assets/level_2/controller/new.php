

    /**
     * @Rest\Post(path="/owners/{ownerId}/xxxxxxs", requirements={"ownerId": "\d+"},
     *     name="api_xxxxxx_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $ownerId
     * @return Response
     */
    public function newAction( Request $request, int $ownerId ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $xxxxxx = new Xxxxxx();
        $xxxxxx->setOwner( $result->getOwner() );

        return $this->processForm( $request, $xxxxxx, true );
    }