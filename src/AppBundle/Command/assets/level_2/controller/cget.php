

    /**
     * @Rest\Get(path="/owners/{ownerId}/xxxxxxs", requirements={"ownerId": "\d+"},
     *     name="api_xxxxxx_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $ownerId
     * @return Response
     */
    public function cgetAction( Request $request, int $ownerId ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $owner = $this->getDoctrine()->getRepository( 'AppBundle:Owner' )->find( $ownerId );
        if ( !$owner ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.owner',
                'message'     => 'Owner was not found',
            ], 404 );
        }

        $xxxxxxs = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->getOwnerXxxxxxs( $ownerId );

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $xxxxxxs as $xxxxxx ) {

                $result[ $xxxxxx->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $xxxxxx->getId() ][ $field ] = $xxxxxx->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxxs, 200 ) );
    }