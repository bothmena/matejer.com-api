

    /**
     * @Rest\Get(path="/owners/{ownerSlug}/xxxxxxs", name="api_xxxxxx_read_all_by_slug",
     *     options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Xxxxxx",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $ownerSlug
     * @return Response
     */
    public function cgetByOwnerSlugAction( Request $request, string $ownerSlug ) {

        $owner = $this->getDoctrine()->getRepository( 'AppBundle:Owner' )->findOneBy( [ 'slug' => $ownerSlug ] );
        if ( !$owner ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.owner',
                'message'     => 'Owner was not found',
            ], 404 );
        }

        $limit = $request->query->getInt( 'limit', 20 );
        $xxxxxxs = $this->getDoctrine()->getRepository( 'AppBundle:Xxxxxx' )
            ->getOwnerXxxxxxs( $ownerSlug );

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $xxxxxxs as $xxxxxx ) {

                $result[ $xxxxxx->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $xxxxxx->getId() ][ $field ] = $xxxxxx->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $xxxxxxs, 200 ) );
    }