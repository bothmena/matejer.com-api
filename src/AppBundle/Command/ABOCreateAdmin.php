<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 21/10/17
 * Time: 13:23
 */

namespace AppBundle\Command;

use AppBundle\Entity\Location;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ABOCreateAdmin extends ContainerAwareCommand {

    protected function configure()
    {

        $this
            // the name of the command (the part after "bin/console")
            ->setName('abo:create:admin')
            // the short description shown while running "php bin/console list"
            ->setDescription('Create an a super admin to manage the website and have full access via API.')
            ->setHelp('This command allows you to generate an admin user.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $output->writeln("Starting creating admin user...");
        $output->writeln("------------------------------------");

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $userManager = $this->getContainer()->get('fos_user.user_manager');

        $address = new Location();
        $address->setCountry('tn');
        $address->setState('Bizerte');
        $address->setCity('Manzel Adb-Rahmen');
        $address->setAddress('my address in some place!');
        $address->setPostalCode('1234');

        $user = $userManager->createUser();
        $user->setEmail('admin@gmail.com');
        $user->setUsername('admin');
        $user->setFirstName('Matejer');
        $user->setLastName('Admin');
        $user->setBirthday(new \DateTime('1988-02-15'));
        $user->setLanguage('ar');
        $user->setGender('male');
        $user->setLocation($address);
        $user->setPlainPassword('adminSTRONG#9564#paSS');
        $user->setEnabled(true);
        $user->addRole('ROLE_SUPER_ADMIN');

        $em->persist($user);
        $em->flush();

        $output->writeln("Done creating admin user");
        $output->writeln("------------------------------------");

    }
}
