<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 21/10/17
 * Time: 13:23
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class ABOGenerateFullAPI extends ContainerAwareCommand {

    private $entityName;
    private $entityPluralName;
    private $parentPluralName;
    private $parentClassName;
    private $entityClassName;
    private $vendorName;
    private $bundleName;
    private $entityPath;
    private $apiType;
    private $methods;
    private $fieldsValues;

    private $patterns = [ "/Xxxxxxs/", "/Xxxxxx/", "/xxxxxxs/", "/xxxxxx/", "/Owners/", "/Owner/",
        "/owners/", "/owner/", "/\[FIELDS\]/", "/'FIELDS'=>'VALUES'/", "/'FIELDS'/" ];
    private $replacements = [];

    protected function configure() {

        $this
            // the name of the command (the part after "bin/console")
            ->setName( 'abo:generate:api' )
            // the short description shown while running "php bin/console list"
            ->setDescription( 'Create an API Controller and ControllerTest for a given Entity.' )
            ->setHelp( 'This command allows you to generate an API for a given Entity, it also help you generate a' .
                'Controller test class and configure the Serializer settings.' );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute( InputInterface $input, OutputInterface $output ) {

        $command = $this->getApplication()->find( 'doctrine:generate:form' );
        $helper = $this->getHelper( 'question' );

        $output->writeln('===== IMPORTANT !!!!! TODO =====');
        $output->writeln('Update level 2 controller annotations: ApiDoc');
        $output->writeln('update: get, cget, getBySlug in level 1 & 2 controller');
        $output->writeln('================================');

        $this->setAPIType( $input, $output, $helper );
        $this->setEntityNamePath( $input, $output, $helper );

        $output->writeln( "-- Generating The Entity Form --" );
        $arguments = [
            'command'          => 'doctrine:generate:form',
            'entity'           => $this->entityName,
            '--no-interaction' => true,
        ];
        $greetInput = new ArrayInput( $arguments );
        $command->run( $greetInput, $output );
        $output->writeln( "-------------------------\n" );

        $this->setMethods( $input, $output, $helper );
        $this->setFieldsValues( $input, $output, $helper );
        $this->setReplacements();

        $this->generateClasses( $output );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $helper
     * @return string
     */
    private function entityPath( InputInterface $input, OutputInterface $output, QuestionHelper $helper ): string {

        $arr = explode( ":", $this->entityName );
        foreach ( $arr as $item ) {

            if ( empty( $item ) ) {

                return NULL;
            }
        }

        // kernel_root_dir/../src[/<vendor>]/<bundle-name>/Entity/<entity-name>.php
        $rootDir = $this->getContainer()->getParameter( 'kernel.root_dir' );
        if ( sizeof( $arr ) === 2 ) {

            $this->vendorName = NULL;
            $this->bundleName = $arr[ 0 ];
            $this->entityClassName = $arr[ 1 ];

            $entityPath = $rootDir . '/../src/' . $arr[ 0 ] . '/Entity/' . $arr[ 1 ] . '.php';
        } else if ( sizeof( $arr ) === 3 ) {

            $this->vendorName = $arr[ 0 ];
            $this->bundleName = $arr[ 1 ];
            $this->entityClassName = $arr[ 2 ];

            $entityPath = $rootDir . '/../src/' . $arr[ 0 ] . '/' . $arr[ 1 ] . '/Entity/' . $arr[ 2 ] . '.php';
        } else {

            return NULL;
        }

        if ( $this->apiType === 'Level 2' ) {

            $question = new Question( 'entity parent Class name: ' );
            $this->parentClassName = $helper->ask( $input, $output, $question );

            $question = new Question( 'entity parent plural name [' . $this->parentClassName . 's]: ' );
            $this->parentPluralName = $helper->ask( $input, $output, $question );
            if ( empty( $this->parentPluralName ) ) {

                $this->parentPluralName = $this->parentClassName . 's';
            }
        }
        $question = new Question( 'entity plural name [' . $this->entityClassName . 's]: ' );
        $this->entityPluralName = $helper->ask( $input, $output, $question );
        if ( empty( $this->entityPluralName ) ) {

            $this->entityPluralName = $this->entityClassName . 's';
        }

        $output->writeln( "$this->entityClassName, $this->entityPluralName, $this->parentClassName, $this->parentPluralName, " );

        return $entityPath;
    }

    /**
     * @param OutputInterface $output
     */
    private function generateClasses( OutputInterface $output ) {

        $output->writeln( "-- Controller & Test classes generation --" );

        $rootPath = $this->getContainer()->getParameter( 'kernel.root_dir' ) . '/../';

        if ( $this->vendorName ) {

            $ctrlFile = fopen( $rootPath . 'src/' . $this->vendorName . '/' . $this->bundleName .
                '/Controller/API/V1/' . $this->entityClassName . 'Controller.php', 'w' );
            $testFile = fopen( $rootPath . 'tests/' . $this->vendorName . '/' . $this->bundleName .
                '/Controller/API/V1/' . $this->entityClassName . 'ControllerTest.php', 'w' );
        } else {

            $ctrlFile = fopen( $rootPath . 'src/' . $this->bundleName .
                '/Controller/API/V1/' . $this->entityClassName . 'Controller.php', 'w' );
            $testFile = fopen( $rootPath . 'tests/' . $this->bundleName .
                '/Controller/API/V1/' . $this->entityClassName . 'ControllerTest.php', 'w' );
        }

        if ( $this->apiType === 'Level 1' ) {

            $partsDir = $rootPath . 'src/AppBundle/Command/assets/level_1/';
        } else if ( $this->apiType === 'Level 2' ) {

            $partsDir = $rootPath . 'src/AppBundle/Command/assets/level_2/';
        } else {

            $output->writeln( 'Wrong level selected!' );

            return;
        }

        $this->addPart( $ctrlFile, $testFile, 'start', $partsDir );
        foreach ( $this->methods as $method ) {

            $this->addPart( $ctrlFile, $testFile, $method, $partsDir );
        }
        if ( in_array( 'new', $this->methods ) || in_array( 'edit', $this->methods ) ) {

            $this->addPart( $ctrlFile, $testFile, 'processForm', $partsDir );
        }
        $this->addPart( $ctrlFile, $testFile, 'end', $partsDir );

        fclose( $ctrlFile );
        fclose( $testFile );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $helper
     */
    private function setEntityNamePath( InputInterface $input, OutputInterface $output, QuestionHelper $helper ) {

        $output->writeln( "-- Entity Shortcut Name --" );
        $srcPath = $this->getContainer()->getParameter( 'kernel.root_dir' ) . '/../src';
        $bundles = scandir( $srcPath );
        $entityAuto = [];
        foreach ( $bundles as $bundle ) {

            if ( !in_array( $bundle, [ '.', '..' ] ) && is_dir( $srcPath . '/' . $bundle ) ) {

                $entities = scandir( $srcPath . '/' . $bundle . '/Entity' );
                foreach ( $entities as $entity ) {

                    if ( substr( $entity, -4 ) === '.php' ) {
                        array_push( $entityAuto, $bundle . ':' . substr( $entity, 0, -4 ) );
                    }
                }
            }
        }

        $entityValid = false;
        while ( !$entityValid ) {

            $output->writeln( 'Valid Entity Shortcut Name: [VendorName:]BundleName:EntityClassName' );
            $output->writeln( 'Exemples: Acme:AcmeBundle:Post or AcmeBundle:Post' );
            $question = new Question( 'The Entity shortcut name: ' );
            $question->setAutocompleterValues( $entityAuto );
            $this->entityName = $helper->ask( $input, $output, $question );
            $this->entityPath = $this->entityPath( $input, $output, $helper );
            if ( $this->entityPath === NULL ) {

                $output->writeln( 'Entity shortcut name is not valid.' );
            } else if ( !file_exists( $this->entityPath ) ) {

                $output->writeln( "entity path: $this->entityPath" );
                $output->writeln( [ 'The entity class file does not exist!', 'Please make sure you did not make any typo.', ] );
            } else {

                $entityValid = true;
            }
        }
        $output->writeln( "-------------------------\n" );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $helper
     */
    private function setAPIType( InputInterface $input, OutputInterface $output, QuestionHelper $helper ) {

        $output->writeln( "-- API Type Selection --" );
        $output->writeln( "INFO:" );
        $output->writeln( "Level 1: /entities" );
        $output->writeln( "Level 2: /parent_entities/{id}/entities" );
        $question = new ChoiceQuestion(
            'Select the type of the API? [Level 1]',
            [ 'Level 1', 'Level 2' ],
            0
        );
        $question->setErrorMessage( 'Please select a valid option.' );
        $this->apiType = $helper->ask( $input, $output, $question );
        $output->writeln( "-------------------------\n" );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $helper
     */
    private function setMethods( InputInterface $input, OutputInterface $output, QuestionHelper $helper ) {

        $output->writeln( "-- API Methods Selection --" );
        $question = new ChoiceQuestion(
            'Select the methods for this API? [get, cget, new, edit, delete]',
            [ 'All', 'get', 'getBySlug', 'cget', 'new', 'edit', 'delete' ],
            '1,3,4,5,6'
        );
        $question->setErrorMessage( 'Please select a valid option.' );
        $question->setMultiselect( true );

        $this->methods = $helper->ask( $input, $output, $question );
        if ( in_array( 'All', $this->methods ) ) {
            $this->methods = [ 'get', 'getBySlug', 'cget', 'new', 'edit', 'delete' ];
        }
        $output->writeln( "-------------------------\n" );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $helper
     */
    private function setFieldsValues( InputInterface $input, OutputInterface $output, QuestionHelper $helper ) {

        $output->writeln( "-- Entity Fields & Values --" );
        $output->writeln( 'Please enter the entity fields and values:' );
        $this->fieldsValues = [];
        while ( true ) {

            $question = new Question( 'entity field name: ' );
            $fieldName = $helper->ask( $input, $output, $question );
            if ( empty( $fieldName ) ) {
                break;
            }
            $question = new Question( 'entity field Value: ' );
            $fieldValue = $helper->ask( $input, $output, $question );
            $this->fieldsValues[ $fieldName ] = $fieldValue;
        }
        $output->writeln( "-------------------------\n" );
    }

    private function addPart( $ctrlFile, $testFile, $method, $partsDir ) {

        // controller
        $partFile = fopen( $partsDir . 'controller/' . $method . '.php', 'r' );
        while ( !feof( $partFile ) ) {
            $line = fgets( $partFile );
            $line = preg_replace( $this->patterns, $this->replacements, $line );
            fwrite( $ctrlFile, $line );
        }
        fclose( $partFile );

        // test
        if ( $method != 'processForm' ) {

            $partFile = fopen( $partsDir . 'test/' . $method . '.php', 'r' );
            while ( !feof( $partFile ) ) {
                $line = fgets( $partFile );
                $line = preg_replace( $this->patterns, $this->replacements, $line );
                fwrite( $testFile, $line );
            }
            fclose( $partFile );
        }
    }

    private function setReplacements() {

        $this->replacements[ 0 ] = ucfirst( $this->entityPluralName );
        $this->replacements[ 1 ] = ucfirst( $this->entityClassName );
        $this->replacements[ 2 ] = strtolower( $this->entityPluralName );
        $this->replacements[ 3 ] = strtolower( $this->entityClassName );

        $this->replacements[ 4 ] = ucfirst( $this->parentPluralName );
        $this->replacements[ 5 ] = ucfirst( $this->parentClassName );
        $this->replacements[ 6 ] = strtolower( $this->parentPluralName );
        $this->replacements[ 7 ] = strtolower( $this->parentClassName );

        $this->replacements[ 8 ] = "";
        $this->replacements[ 9 ] = "";
        $this->replacements[ 10 ] = "";
        foreach ( $this->fieldsValues as $field => $value ) {

            $serializedField = $this->getSerializedField( $field );
            $this->replacements[ 8 ] .= "$serializedField, ";
            $this->replacements[ 9 ] .= "'$field' => '$value', ";
            $this->replacements[ 10 ] .= "'$serializedField', ";
        }
        $this->replacements[ 8 ] = substr( $this->replacements[ 8 ], 0, -2 );
        $this->replacements[ 9 ] = substr( $this->replacements[ 9 ], 0, -2 );
        $this->replacements[ 10 ] = substr( $this->replacements[ 10 ], 0, -2 );
    }

    private function getSerializedField( string $field ): string {

        $strlen = strlen( $field );
        $serializedField = '';
        for ( $i = 0; $i <= $strlen; $i++ ) {
            $char = substr( $field, $i, 1 );
            if ( ( $char >= '0' && $char <= '9' ) || ( $char >= 'a' && $char <= 'z' ) ) {

                $serializedField .= mb_strtolower($char);
            } else if ( $char >= 'A' && $char <= 'Z' ) {

                if ( $i > 0 && substr( $field, $i-1, 1 ) !== '_' ) {
                    $serializedField .= '_';
                }
                $serializedField .= mb_strtolower($char);
            } else if ( $char === '_' && ( $i === 0 || substr( $field, $i-1, 1 ) !== '_' ) ) {

                $serializedField .= $char;
            }
        }
        return $serializedField;
    }
}
