<?php

namespace AppBundle\Form\Shop;

use AppBundle\Entity\Category;
use AppBundle\Entity\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotNull;

class CollectionType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'name', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'level', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual( [ 'value' => 1 ] ),
                    new LessThanOrEqual( [ 'value' => 3 ] ),
                ],
            ] )
            ->add( 'category', EntityType::class, [
                'class' => Category::class,
            ] )
            ->add( 'anyParent', CheckboxType::class, [
                'constraints' => [
                    new NotNull(),
                ],
            ] )
            ->add( 'parent', EntityType::class, [
                'class' => Collection::class,
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'AppBundle\Entity\Collection',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_collection';
    }

}
