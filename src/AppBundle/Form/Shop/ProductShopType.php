<?php

namespace AppBundle\Form\Shop;

use AppBundle\Entity\CategoryProduct;
use AppBundle\Entity\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotNull;

class ProductShopType extends AbstractType {

    private $availibility = [ 'stock', 'comnd', 'avasn', 'unava' ];

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'reference', TextType::class, [
                'constraints' => [
                    new Length( [ 'min' => 3, 'max' => 21 ] ),
                ],
            ] )
            ->add( 'price', NumberType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThan( [ 'value' => 0 ] ),
                ],
            ] )
            ->add( 'discount', NumberType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual( [ 'value' => 0 ] ),
                    new LessThanOrEqual( [ 'value' => 100 ] ),
                ],
            ] )
            ->add( 'warranty', NumberType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual( [ 'value' => 0 ] ),
                ],
            ] )
            ->add( 'availability', ChoiceType::class, [
                'choices' => $this->availibility,
                'constraints' => [
                    new NotNull(),
                    new Choice(['choices'=>$this->availibility])
                ]
            ] )
            ->add( 'categoryProduct', EntityType::class, [
                'class' => CategoryProduct::class,
                'constraints' => [
                    new NotNull(),
                ]
            ]  )
            ->add('collection', EntityType::class, array(
                'class' => Collection::class,
                'mapped' => false,
                'empty_data' => array(),
                'multiple' => true,
            ))
            /**
             * @todo add payments field.
             */
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'AppBundle\Entity\ProductShop',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_productshop';
    }

}
