<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: Aymen Ben Othmen
 * Date: 04/04/17
 * Time: 08:22
 */
class DateTimeToStringTransformer implements DataTransformerInterface {

    private $format;
    private $shouldLessThanToday;

    public function __construct( string $format = 'd/m/Y H:i:s', bool $shouldLessThanToday = false ) {

        $this->format = $format;
        $this->shouldLessThanToday = $shouldLessThanToday;
    }

    /**
     * @param \DateTime $dateTime
     * @return mixed|string
     */
    public function transform ( $dateTime ): string {

        try {
            return $dateTime->format($this->format);
        } catch ( \Exception|\Throwable $e ) {
            return '';
        }
    }

    /**
     * @param string $string
     * @return \DateTime|mixed
     */
    public function reverseTransform ( $string ) {

        try {
            return \DateTime::createFromFormat( $this->format, $string );

        } catch ( \Exception|\Throwable $e ) {
            throw new TransformationFailedException( sprintf( 'Invalid datetime format given "%s"', $string ) );
        }
    }
}
