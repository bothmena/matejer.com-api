<?php

namespace AppBundle\Form\Product;

use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryProduct;
use AppBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Valid;

class CategoryProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', ProductType::class, [
                'constraints' => [
                    new Valid(),
                ],
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('generalSpecs', GeneralSpecType::class, [
                'constraints' => [
                    new Valid(),
                ],
            ])
            ->add('shop', EntityType::class, [
                'class' => Shop::class,
                'required' => false,
            ])
            ->add('anyParent', CheckboxType::class, [
                'required' => false,
            ])
            ->add('parent', EntityType::class, [
                'class' => CategoryProduct::class,
                'required' => false,
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CategoryProduct'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_cat_prod';
    }


}
