<?php

namespace AppBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class ProductType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'reference', TextType::class, [
                'constraints' => [
                    new Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'name', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'mark', TextType::class, [
                'constraints' => [
                    new Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'public', CheckboxType::class, [
                'constraints' => [
                    new NotNull(),
                ],
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'AppBundle\Entity\Product',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_product';
    }

}
