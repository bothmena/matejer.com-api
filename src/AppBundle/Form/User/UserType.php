<?php

namespace AppBundle\Form\User;

use AppBundle\Form\DataTransformer\DateTimeToStringTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'username', Type\TextType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                    new Assert\Regex( [ 'pattern' => '/^[a-zA-Z]{1}[\w.-]+$/' ] ),
                ],
            ] )
            ->add( 'email', Type\EmailType::class, [
                'constraints' => [
                    new Assert\Email(), //['checkMX'=>true, 'checkHost'=>true]
                ],
            ] )
            ->add( 'firstName', Type\EmailType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'lastName', Type\EmailType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add('birthday', Type\TextType::class, [] )
            ->add( 'plainPassword', Type\RepeatedType::class, [
                    'required'        => true,
                    'type'            => Type\PasswordType::class,
                    'first_name'      => 'password',
                    'second_name'     => 'confirm_password',
                    'first_options'   => [
                        'constraints' => [
                            new Assert\Regex( [
                                'pattern' => '/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
                                'message' => 'password must contain at least 1 number, 1 uppercase, 1, lowercase',
                            ] ),
                        ],
                    ],
                    'invalid_message' => 'The password fields must match.',
                ]
            )
            ;

        $builder->get('birthday')
            ->addModelTransformer(new DateTimeToStringTransformer( 'd/m/Y' ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'AppBundle\Entity\User',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_user';
    }

}
