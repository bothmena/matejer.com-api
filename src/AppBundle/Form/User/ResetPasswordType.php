<?php

namespace AppBundle\Form\User;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'oldPassword', Type\PasswordType::class, [
                'mapped' => false,
                'constraints' => [
                    new Assert\NotNull(),
                    new UserPassword()
                ]
            ] )
            ->add( 'plainPassword', Type\RepeatedType::class, [
                    'required'        => true,
                    'type'            => Type\PasswordType::class,
                    'first_name'      => 'password',
                    'second_name'     => 'confirm_password',
                    'first_options'   => [
                        'constraints' => [
                            new Assert\Regex( [
                                'pattern' => '/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
                                'message' => 'password must contain at least 1 number, 1 uppercase, 1, lowercase',
                            ] ),
                        ],
                    ],
                    'invalid_message' => 'The password fields must match.',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'AppBundle\Entity\User',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_user_reset_pass';
    }

}
