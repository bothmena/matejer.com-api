<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 08/02/17
 * Time: 19:56
 */

namespace AppBundle;

final class AppEvents {

    const REGISTRATION_SUCCESS = 'mtjr.regis_succ';
    const USER_CONFIRM_EMAIL_SUCCESS = 'mtjr.usr_cnfrm_mail';

    const SHOP_IMAGE_UPLOAD_SUCCESS = 'mtjr.shop_img_upload_success';
    const USER_IMAGE_UPLOAD_SUCCESS = 'mtjr.user_img_upload_success';
    const PRODUCT_IMAGE_UPLOAD_SUCCESS = 'mtjr.prod_img_upload_success';
}
