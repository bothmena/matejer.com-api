<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 10/12/17
 * Time: 14:04
 */

namespace AppBundle\Event;

use AppBundle\Entity\Image;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserImageUploadSuccessEvent extends Event {

    private $image;
    private $user;
    private $response;

    public function __construct( User $user, Image $image ) {

        $this->image = $image;
        $this->user = $user;
    }

    /**
     * @return Image
     */
    public function getImage(): Image {

        return $this->image;
    }

    /**
     * @return User
     */
    public function getUser(): User {

        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getResponse() {

        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse( $response ) {

        $this->response = $response;
    }
}
