<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 10/12/17
 * Time: 14:02
 */

namespace AppBundle\Event;

use AppBundle\Entity\Image;
use AppBundle\Entity\Shop;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

class ShopImageUploadSuccessEvent extends Event {

    private $image;
    private $shop;
    private $response;

    public function __construct( Shop $shop, Image $image ) {

        $this->image = $image;
        $this->shop = $shop;
    }

    /**
     * @return Image
     */
    public function getImage(): Image {

        return $this->image;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop {

        return $this->shop;
    }

    /**
     * @param mixed $response
     */
    public function setResponse( Response $response ) {

        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getResponse() {

        return $this->response;
    }
}
