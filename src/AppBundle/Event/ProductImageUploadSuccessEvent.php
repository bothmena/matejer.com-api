<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 10/12/17
 * Time: 14:04
 */

namespace AppBundle\Event;

use AppBundle\Entity\Image;
use AppBundle\Entity\CategoryProduct;
use Symfony\Component\EventDispatcher\Event;

class ProductImageUploadSuccessEvent extends Event {

    private $image;
    private $categoryProduct;
    private $response;

    public function __construct( CategoryProduct $categoryProduct, Image $image ) {

        $this->image = $image;
        $this->categoryProduct = $categoryProduct;
    }

    /**
     * @return Image
     */
    public function getImage(): Image {

        return $this->image;
    }

    /**
     * @return CategoryProduct
     */
    public function getCategoryProduct(): CategoryProduct {

        return $this->categoryProduct;
    }

    /**
     * @return mixed
     */
    public function getResponse() {

        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse( $response ) {

        $this->response = $response;
    }
}
