<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 16/11/17
 * Time: 20:10
 */

namespace AppBundle\Utils;

use AppBundle\Entity\Shop;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

class AccessCheckerResult {

    /**
     * @var User $user
     * @var Shop $shop
     * @var Response $response
     */
    private $user;
    private $shop;
    private $response;

    /**
     * AccessCheckerResult constructor.
     */
    public function __construct() {
    }

    /**
     * @return User|UserInterface
     */
    public function getUser() {

        return $this->user;
    }

    /**
     * @param UserInterface $user
     */
    public function setUser( UserInterface $user ) {

        $this->user = $user;
    }

    /**
     * @return Shop
     */
    public function getShop() {

        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop( Shop $shop ) {

        $this->shop = $shop;
    }

    /**
     * @return Response
     */
    public function getResponse() {

        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse( Response $response ) {

        $this->response = $response;
    }
}