<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 16/11/17
 * Time: 20:08
 */

namespace AppBundle\Service;

use AppBundle\Utils\AccessCheckerResult;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ABOAccessChecker {

    private $manager;
    private $authChecker;
    private $tokenStorage;

    public function __construct( EntityManager $manager, AuthorizationChecker $checker, TokenStorage $tokenStorage ) {

        $this->manager = $manager;
        $this->authChecker = $checker;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param int $shopId
     * @return AccessCheckerResult
     */
    public function checkForShop( int $shopId ): AccessCheckerResult {

        $result = new AccessCheckerResult();

        if ( false === $this->authChecker->isGranted( 'ROLE_USER' ) ) {

            $result->setResponse(
                new JsonResponse( [
                    'status_code' => 401,
                    'error_code'  => 'not_authenticated',
                    'message'     => 'you are not authenticated',
                ], 401 )
            );

            return $result;
        }

        if ( false === $this->authChecker->isGranted( 'ROLE_TAJER' ) ) {

            $result->setResponse(
                new JsonResponse( [
                    'status_code' => 403,
                    'error_code'  => 'access_denied.role_not_granted',
                    'message'     => 'you are not authorised',
                ], 403 )
            );

            return $result;
        }

        $shop = $this->manager->getRepository( 'AppBundle:Shop' )->find( $shopId );
        if ( !$shop ) {

            $result->setResponse(
                new JsonResponse( [
                    'status_code' => 404,
                    'error_code'  => 'not_found.shop',
                    'message'     => 'Shop was not found',
                ], 404 )
            );

            return $result;
        }

        $result->setShop( $shop );
        $user = $this->tokenStorage->getToken()->getUser();
        $result->setUser( $user );
        /**
         * @var $user \AppBundle\Entity\User
         */
        if ( $user->getShopId() !== $shop->getId() ) {

            $result->setResponse(
                new JsonResponse( [
                    'status_code' => 403,
                    'error_code'  => 'access_denied.not_user_property',
                    'message'     => 'you are not authorised',
                ], 403 )
            );

            return $result;
        }

        return $result;
    }

    /**
     * @param int $userId
     * @param string|null $role
     * @return AccessCheckerResult
     */
    public function checkForUser( string $role, int $userId = NULL ): AccessCheckerResult {

        $result = new AccessCheckerResult();

        if ( false === $this->authChecker->isGranted( 'ROLE_USER' ) ) {

            $result->setResponse(
                new JsonResponse( [
                    'status_code' => 401,
                    'error_code'  => 'not_authenticated',
                    'message'     => 'you are not authenticated',
                ], 401 )
            );

            return $result;
        }

        if ( $role ) {

            if ( false === $this->authChecker->isGranted( $role ) ) {

                $result->setResponse(
                    new JsonResponse( [
                        'status_code' => 403,
                        'error_code'  => 'access_denied.role_not_granted',
                        'message'     => 'you are not authorised',
                    ], 403 )
                );

                return $result;
            }
        }

        if ( $userId ) {
            $user = $this->manager->getRepository( 'AppBundle:User' )->find( $userId );
            if ( !$user ) {

                $result->setResponse(
                    new JsonResponse( [
                        'status_code' => 404,
                        'error_code'  => 'not_found.user',
                        'message'     => 'User was not found',
                    ], 404 )
                );

                return $result;
            }
            $result->setUser( $user );

            if ( $user->getId() !== $this->tokenStorage->getToken()->getUser()->getId() ) {

                $result->setResponse(
                    new JsonResponse( [
                        'status_code' => 403,
                        'error_code'  => 'access_denied.not_user_property',
                        'message'     => 'you are not authorised',
                    ], 403 )
                );

                return $result;
            }
        } else {

            $result->setUser( $this->tokenStorage->getToken()->getUser() );
        }

        return $result;
    }
}
