<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CategoryShop
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CategoryShop {
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category")
     * @ORM\JoinColumn(nullable=false)
     */
   private $category;
   
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Shop")
     * @ORM\JoinColumn(nullable=false)
     */
   private $shop;

    /**
     * @var integer
     *
     * @ORM\Column(name="productNb", type="integer", options={"unsigned"=true})
     */
    private $productNb;

    public function __construct( Category $category = null, Shop $shop = null ) {

        $this->category = $category;
        $this->shop = $shop;
        $this->productNb = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return CategoryShop
     */
    public function setCategory(\AppBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set shop
     *
     * @param \AppBundle\Entity\Shop $shop
     *
     * @return CategoryShop
     */
    public function setShop(\AppBundle\Entity\Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \AppBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set productNb
     *
     * @param integer $productNb
     *
     * @return CategoryShop
     */
    public function setProductNb($productNb)
    {
        $this->productNb = $productNb;

        return $this;
    }

    /**
     * Get productNb
     *
     * @return integer
     */
    public function getProductNb()
    {
        return $this->productNb;
    }
}
