<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductColor
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ProductColor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Color")
    * @ORM\JoinColumn(nullable=false)
    */
    private $color;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CategoryProduct")
    * @ORM\JoinColumn(nullable=false)
    */
    private $categoryProduct;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set color
     *
     * @param \AppBundle\Entity\Color $color
     *
     * @return ProductColor
     */
    public function setColor(\AppBundle\Entity\Color $color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \AppBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set categoryProduct
     *
     * @param \AppBundle\Entity\CategoryProduct $categoryProduct
     *
     * @return ProductColor
     */
    public function setCategoryProduct(\AppBundle\Entity\CategoryProduct $categoryProduct)
    {
        $this->categoryProduct = $categoryProduct;

        return $this;
    }

    /**
     * Get categoryProduct
     *
     * @return \AppBundle\Entity\CategoryProduct
     */
    public function getCategoryProduct()
    {
        return $this->categoryProduct;
    }
}
