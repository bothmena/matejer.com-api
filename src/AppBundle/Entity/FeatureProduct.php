<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureProduct
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FeatureProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CategoryProduct")
    * @ORM\JoinColumn(nullable=false)
    */
    private $categoryProduct;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Feature")
    * @ORM\JoinColumn(nullable=false)
    */
    private $feature;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryProduct
     *
     * @param \AppBundle\Entity\CategoryProduct $categoryProduct
     *
     * @return FeatureProduct
     */
    public function setCategoryProduct(\AppBundle\Entity\CategoryProduct $categoryProduct)
    {
        $this->categoryProduct = $categoryProduct;

        return $this;
    }

    /**
     * Get categoryProduct
     *
     * @return \AppBundle\Entity\CategoryProduct
     */
    public function getCategoryProduct()
    {
        return $this->categoryProduct;
    }

    /**
     * Set feature
     *
     * @param \AppBundle\Entity\Feature $feature
     *
     * @return FeatureProduct
     */
    public function setFeature(\AppBundle\Entity\Feature $feature)
    {
        $this->feature = $feature;

        return $this;
    }

    /**
     * Get feature
     *
     * @return \AppBundle\Entity\Feature
     */
    public function getFeature()
    {
        return $this->feature;
    }
}
