<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Shop
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShopRepository")
 */
class Shop {
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Location",cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @ORM\OneToOne(targetEntity="RateStat",cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $rateStat;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     */
    private $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=61)
     */
    private $name;
    
    /**
    * @Gedmo\Slug(fields={"name"})
    * @ORM\Column(length=80, unique=true)
    */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     */
    private $currency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscriptionDate", type="datetime")
     */
    private $inscriptionDate;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=255)
     */
    private $slogan;

    /**
     * @var integer
     *
     * @ORM\Column(name="clientNb", type="integer", options={"unsigned"=true})
     */
    private $clientNb;

    /**
     * @var integer
     *
     * @ORM\Column(name="offerNb", type="integer", options={"unsigned"=true})
     */
    private $offerNb;
    
    public function __construct() {
        
        $this->rateStat = new RateStat();
        $this->location = new Location();
        $this->inscriptionDate = new \DateTime;
        $this->website = '';
        $this->description = '';
        $this->currency = 'tnd';
        $this->clientNb = 0;
        $this->offerNb = 0;
    }

    public function getField( $field ) {

        return '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getClassName() {
        
        return 'ShopEntity';
    }
    
    public function transCurrency() {
        
        return 'matejer_currency.'.$this->currency;
    }

    public function transExCurrency() {

        return 'matejer_ex_currency.'.$this->currency;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Shop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Shop
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Shop
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set inscriptionDate
     *
     * @param \DateTime $inscriptionDate
     *
     * @return Shop
     */
    public function setInscriptionDate($inscriptionDate)
    {
        $this->inscriptionDate = $inscriptionDate;

        return $this;
    }

    /**
     * Get inscriptionDate
     *
     * @return \DateTime
     */
    public function getInscriptionDate()
    {
        return $this->inscriptionDate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Shop
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Shop
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return Shop
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Set clientNb
     *
     * @param integer $clientNb
     *
     * @return Shop
     */
    public function setClientNb($clientNb)
    {
        $this->clientNb = $clientNb;

        return $this;
    }

    /**
     * Get clientNb
     *
     * @return integer
     */
    public function getClientNb()
    {
        return $this->clientNb;
    }

    /**
     * Set address
     *
     * @param Location $location
     *
     * @return Shop
     */
    public function setLocation( Location $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get address
     *
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set rateStat
     *
     * @param RateStat $rateStat
     *
     * @return Shop
     */
    public function setRateStat( RateStat $rateStat)
    {
        $this->rateStat = $rateStat;

        return $this;
    }

    /**
     * Get rateStat
     *
     * @return RateStat
     */
    public function getRateStat()
    {
        return $this->rateStat;
    }

    /**
     * Set image
     *
     * @param Image $image
     *
     * @return Shop
     */
    public function setImage( Image $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set offerNb
     *
     * @param integer $offerNb
     *
     * @return Shop
     */
    public function setOfferNb($offerNb)
    {
        $this->offerNb = $offerNb;

        return $this;
    }

    /**
     * Get offerNb
     *
     * @return integer
     */
    public function getOfferNb()
    {
        return $this->offerNb;
    }

    /**
     * Set cover
     *
     * @param Image $cover
     *
     * @return Shop
     */
    public function setCover( Image $cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return Image
     */
    public function getCover()
    {
        return $this->cover;
    }
}
