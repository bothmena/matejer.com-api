<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"email"},message="user.email_duplicated")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class User extends BaseUser {

    /**
     * @ORM\PrePersist
     */
    public function changeUsername() {

        if ( $this->username === '' || $this->username === NULL ) {
            $this->username = $this->email;
            $this->usernameCanonical = $this->emailCanonical;
        }
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $email;

    /**
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $username;

    /**
     * @ORM\OneToOne(targetEntity="Shop")
     * @var $image Shop
     */
    private $myShop;

    /**
     * @ORM\OneToOne(targetEntity="Location",cascade={"persist","remove"})
     * @var $image Location
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     * @var $image Image
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=31)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=31)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="newEmail", type="string", length=255, nullable=true)
     */
    private $newEmail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscriptionDate", type="datetime")
     */
    private $inscriptionDate;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=6, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=2, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="facebook_id", type="bigint", options={"unsigned"=true}, nullable=true)
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $facebook_id;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $facebookAccessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="google_id", type="string", length=255, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $google_id;

    /**
     * @var string
     *
     * @ORM\Column(name="google_access_token", type="string", length=255, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $googleAccessToken;

    public function __construct() {

        parent::__construct();
        $this->inscriptionDate = new \DateTime;
        $this->location = new Location();
        $this->language = 'fr';
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("image_url")
     *
     * @return string|NULL
     */
    public function getImageUrl() {

        return $this->image ? $this->image->getSource() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("location_id")
     *
     * @return int|NULL
     */
    public function getLocationId() {

        return $this->location ? $this->location->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("birthday")
     *
     * @return string|NULL
     */
    public function getBirthdayStr() {

        return $this->birthday ? $this->birthday->format( 'd/m/Y' ) : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("role")
     *
     * @return string
     */
    public function getRole(): string {

        if ( empty($this->roles) ) {
            return 'user';
        }
        return strtolower( substr( $this->roles[ 0 ], 5 ) );
    }

    /**
     * @IsTrue(message = "birthday is not valid, user age should be between 12 and 100 years old!")
     */
    public function isBirthdayValid(): bool {

        if ( $this->birthday ) {
            return ( $this->birthday->diff( new \DateTime() )->y >= 12 &&
                $this->birthday->diff( new \DateTime() )->y <= 100 );
        } else {
            return true;
        }
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("shop_id")
     *
     * @return int|NULL
     */
    public function getShopId() {

        return $this->myShop ? $this->myShop->getId() : NULL;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname() {

        if ( $this->username === $this->email )
            return $this->lastName . ' ' . $this->firstName;
        else
            return $this->username;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->lastName . ' ' . $this->firstName;
    }

    public function confirm() {

        $this->confirmationToken = '';
        $this->locked = false;

        return $this;
    }

    public function getClassName() {

        return 'UserEntity';
    }

    public function isPrimaryEmailConfirmed() {

        if ( !empty( $this->newEmail ) )
            return true;
        else if ( $this->confirmationToken === NULL )
            return true;
        else if ( $this->expiresAt === NULL )
            return true;
        else
            return false;
    }

    public function isEmailConfirmed() {

        if ( $this->confirmationToken === NULL )
            return true;
        else if ( $this->expiresAt === NULL )
            return true;
        else
            return false;
    }

    /**
     * Set firstname
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName( $firstName ) {

        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstName() {

        return $this->firstName;
    }

    /**
     * Set lastname
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName( $lastName ) {

        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastName() {

        return $this->lastName;
    }

    /**
     * Set newEmail
     *
     * @param string $newEmail
     *
     * @return User
     */
    public function setNewEmail( $newEmail ) {

        $this->newEmail = $newEmail;

        return $this;
    }

    /**
     * Get newEmail
     *
     * @return string
     */
    public function getNewEmail() {

        return $this->newEmail;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday( $birthday ) {

        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday() {

        return $this->birthday;
    }

    /**
     * Set inscriptionDate
     *
     * @param \DateTime $inscriptionDate
     *
     * @return User
     */
    public function setInscriptionDate( $inscriptionDate ) {

        $this->inscriptionDate = $inscriptionDate;

        return $this;
    }

    /**
     * Get inscriptionDate
     *
     * @return \DateTime
     */
    public function getInscriptionDate() {

        return $this->inscriptionDate;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender( $gender ) {

        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender() {

        return $this->gender;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return User
     */
    public function setLanguage( $language ) {

        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage() {

        return $this->language;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken( $facebookAccessToken ) {

        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken() {

        return $this->facebookAccessToken;
    }

    /**
     * Set googleAccessToken
     *
     * @param string $googleAccessToken
     *
     * @return User
     */
    public function setGoogleAccessToken( $googleAccessToken ) {

        $this->googleAccessToken = $googleAccessToken;

        return $this;
    }

    /**
     * Get googleAccessToken
     *
     * @return string
     */
    public function getGoogleAccessToken() {

        return $this->googleAccessToken;
    }

    /**
     * Set myShop
     *
     * @param Shop $myShop
     *
     * @return User
     */
    public function setMyShop( Shop $myShop = NULL ) {

        $this->myShop = $myShop;

        return $this;
    }

    /**
     * Get myShop
     *
     * @return Shop
     */
    public function getMyShop() {

        return $this->myShop;
    }

    /**
     * Set location
     *
     * @param Location $location
     *
     * @return User
     */
    public function setLocation( Location $location = NULL ) {

        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return Location
     */
    public function getLocation() {

        return $this->location;
    }

    /**
     * Set image
     *
     * @param Image $image
     *
     * @return User
     */
    public function setImage( Image $image = NULL ) {

        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Image
     */
    public function getImage() {

        return $this->image;
    }

    /**
     * Set facebook_id
     *
     * @param $facebookId
     * @return User
     * @internal param int $facebook_id
     *
     */
    public function setFacebookId( $facebookId ) {

        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebook_id
     *
     * @return integer
     */
    public function getFacebookId() {

        return $this->facebook_id;
    }

    /**
     * Set googleId
     *
     * @param string $googleId
     *
     * @return User
     */
    public function setGoogleId( $googleId ) {

        $this->google_id = $googleId;

        return $this;
    }

    /**
     * Get googleId
     *
     * @return string
     */
    public function getGoogleId() {

        return $this->google_id;
    }
}
