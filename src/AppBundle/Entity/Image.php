<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Image {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=10)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=60, unique=true)
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    protected $file;
    protected $files;

    public function __construct() {

        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {

        return $this->id;
    }

    public function getFileExt() {

        if ( NULL === $this->file ) {
            return NULL;
        } else {
            $extension = $this->file->guessExtension();
            if ( !$extension ) {
                $extension = 'bin';
            }
        }

        return $extension;
    }

    public function upload() {

        if ( NULL === $this->file ) {
            return;
        }

        $this->date = new \DateTime();

        $this->file->move( $this->getUploadRootDir(), $this->image );
    }

    public function getUploadDir() {

        return 'uploads/' . $this->entity;
    }

    public function getUploadRootDir() {

        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    /**
     * Get rootSource
     *
     */
    public function getRootSource() {

        return $this->getUploadRootDir() . '/' . $this->image;
    }

    /**
     * Get source
     *
     */
    public function getSource() {

        return $this->getUploadDir() . '/' . $this->image;
    }

    /**
     * Set file
     *
     * @return Image
     */
    public function setFile( $file ) {

        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     */
    public function getFile() {

        return $this->file;
    }

    /**
     * Set files
     *
     * @return Product
     */
    public function setFiles( $files ) {

        $this->files = $files;

        return $this;
    }

    /**
     * Get files
     *
     */
    public function getFiles() {

        return $this->files;
    }

    /**
     * Set entity
     *
     * @param string $entity
     *
     * @return Image
     */
    public function setEntity( $entity ) {

        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string
     */
    public function getEntity() {

        return $this->entity;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Image
     */
    public function setType( $type ) {

        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Image
     */
    public function setImage( $image ) {

        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {

        return $this->image;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Image
     */
    public function setDate( $date ) {

        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {

        return $this->date;
    }
}
