<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryProduct
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryProductRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CategoryProduct {
    

    /**
     * @ORM\PrePersist
     */
    public function markSpecsClass(){
    
        /*if($this->trademark)
            $this->product->setMark('');*/
//        $this->product->checkReference();
        $this->generalSpecs->setSpecsClass($this->category->getSpecsClass());
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Product", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var $category Category
     */
    private $category;
    
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\GeneralSpec", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     *
     * @var $generalSpecs GeneralSpec
     */
    private $generalSpecs;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Shop")
     */
    private $shop;
    
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\RateStat",cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $rateStat;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Image",cascade={"remove"})
     * @ORM\JoinColumn(name="productImage")
     */
    private $image;

    /**
     * @var boolean
     *
     * @ORM\Column(name="anyParent", type="boolean")
     */
    private $anyParent;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CategoryProduct")
     */
    private $parent;


    /**
     * Constructor
     */
    public function __construct() {
        
        $this->rateStat = new RateStat();
        $this->product = new Product();
        $this->anyParent = false;
    }
    
    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName() {
        
        /*if($this->trademark)
            $name = $this->trademark->getName().' - ';
        else*/
            $name = $this->product->getMark().' - ';
        
        if( empty( $this->product->getName() ) )
            return $name.$this->product->getReference();
        else
            return $name.$this->product->getName();
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand() {

//        if($this->trademark !== null)
//            return $this->trademark->getName();
//        else
            return $this->product->getMark();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return CategoryProduct
     */
    public function setProduct(\AppBundle\Entity\Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return CategoryProduct
     */
    public function setCategory(\AppBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set generalSpecs
     *
     * @param \AppBundle\Entity\GeneralSpec $generalSpecs
     *
     * @return CategoryProduct
     */
    public function setGeneralSpecs(\AppBundle\Entity\GeneralSpec $generalSpecs)
    {
        $this->generalSpecs = $generalSpecs;

        return $this;
    }

    /**
     * Get generalSpecs
     *
     * @return \AppBundle\Entity\GeneralSpec
     */
    public function getGeneralSpecs()
    {
        return $this->generalSpecs;
    }

    /**
     * Set shop
     *
     * @param \AppBundle\Entity\Shop $shop
     *
     * @return CategoryProduct
     */
    public function setShop(\AppBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \AppBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set rateStat
     *
     * @param \AppBundle\Entity\RateStat $rateStat
     *
     * @return CategoryProduct
     */
    public function setRateStat(\AppBundle\Entity\RateStat $rateStat)
    {
        $this->rateStat = $rateStat;

        return $this;
    }

    /**
     * Get rateStat
     *
     * @return \AppBundle\Entity\RateStat
     */
    public function getRateStat()
    {
        return $this->rateStat;
    }

    /**
     * Set image
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return CategoryProduct
     */
    public function setImage(\AppBundle\Entity\Image $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \AppBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\CategoryProduct $parent
     *
     * @return CategoryProduct
     */
    public function setParent(\AppBundle\Entity\CategoryProduct $parent = null) {

        $this->setAnyParent(false);
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\CategoryProduct
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set anyParent
     *
     * @param boolean $anyParent
     *
     * @return CategoryProduct
     */
    public function setAnyParent($anyParent)
    {
        $this->anyParent = $anyParent;

        return $this;
    }

    /**
     * Get anyParent
     *
     * @return boolean
     */
    public function getAnyParent()
    {
        return $this->anyParent;
    }
}
