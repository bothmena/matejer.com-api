<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 20/12/16
 * Time: 13:34
 */

namespace AppBundle\Entity;

use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken as BaseRefreshToken;

class RefreshToken extends BaseRefreshToken {}
