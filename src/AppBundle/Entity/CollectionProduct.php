<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 21/12/17
 * Time: 01:14
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CollectionProduct
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CollectionProduct {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Collection")
     * @ORM\JoinColumn(nullable=false)
     */
    private $collection;

    /**
     * @ORM\ManyToOne(targetEntity="ProductShop")
     * @ORM\JoinColumn(nullable=false)
     */
    private $productShop;

    public function __construct( Collection $collection, ProductShop $productShop ) {

        $this->collection = $collection;
        $this->productShop = $productShop;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set collection
     *
     * @param Collection $collection
     *
     * @return CollectionProduct
     */
    public function setCollection( Collection $collection ) {

        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection
     *
     * @return Collection
     */
    public function getCollection() {

        return $this->collection;
    }

    /**
     * Set productShop
     *
     * @param ProductShop $productShop
     *
     * @return CollectionProduct
     */
    public function setProductShop( ProductShop $productShop ) {

        $this->productShop = $productShop;

        return $this;
    }

    /**
     * Get productShop
     *
     * @return ProductShop
     */
    public function getProductShop() {

        return $this->productShop;
    }
}
