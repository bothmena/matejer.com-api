<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Collection
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CollectionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Collection {

    /**
     * @ORM\PrePersist
     */
    public function settingLevel(){
        if($this->getParent())
            $this->setLevel($this->getParent()->getLevel() + 1);
        else
            $this->setLevel(1);
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\ManyToOne(targetEntity="Category")
    */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Collection")
     */
    private $parent;
    
    /**
    * @ORM\ManyToOne(targetEntity="Shop")
    * @ORM\JoinColumn(nullable=false)
    */
    private $shop;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=61)
     */
    private $name;
    
    /**
    * @Gedmo\Slug(fields={"name"})
    * @ORM\Column(length=80, unique=true)
    */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="productNb", type="integer", options={"unsigned"=true})
     */
    private $productNb;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="smallint")
     */
    private $level;

    /**
     * @var boolean
     *
     * @ORM\Column(name="anyParent", type="boolean")
     */
    private $anyParent;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct() {
        
        $this->date = new \DateTime;
        $this->productNb = 0;
        $this->anyParent = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Collection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Collection
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Collection
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Collection
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set shop
     *
     * @param \AppBundle\Entity\Shop $shop
     *
     * @return Collection
     */
    public function setShop(\AppBundle\Entity\Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \AppBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set productNb
     *
     * @param integer $productNb
     *
     * @return Collection
     */
    public function setProductNb($productNb)
    {
        $this->productNb = $productNb;

        return $this;
    }

    /**
     * Get productNb
     *
     * @return integer
     */
    public function getProductNb()
    {
        return $this->productNb;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Collection
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set anyParent
     *
     * @param boolean $anyParent
     *
     * @return Collection
     */
    public function setAnyParent($isParent)
    {
        $this->anyParent = $isParent;

        return $this;
    }

    /**
     * Get anyParent
     *
     * @return boolean
     */
    public function getAnyParent()
    {
        return $this->anyParent;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Collection $parent
     *
     * @return Collection
     */
    public function setParent(\AppBundle\Entity\Collection $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Collection
     */
    public function getParent()
    {
        return $this->parent;
    }
}
