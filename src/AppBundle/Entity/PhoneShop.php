<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PhoneShop
 *
 * @ORM\Table()
 * @ORM\Entity
 * @UniqueEntity(fields="subscriber", message="This phone number is taken.")
 */
class PhoneShop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Shop")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneCode", type="string", length=6)
     */
    private $phoneCode;

    /**
     * @var string
     *
     * @ORM\Column(name="subscriber", type="string", length=30, unique=true)
     */
    private $subscriber;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get full
     *
     * @return string
     */
    public function getFull()
    {
        return '(' . $this->phoneCode . ') ' . $this->subscriber;
    }

    /**
     * Set phoneCode
     *
     * @param string $phoneCode
     *
     * @return PhoneShop
     */
    public function setPhoneCode($phoneCode)
    {
        $this->phoneCode = $phoneCode;

        return $this;
    }

    /**
     * Get phoneCode
     *
     * @return string
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    /**
     * Set subscriber
     *
     * @param string $subscriber
     *
     * @return PhoneShop
     */
    public function setSubscriber($subscriber)
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * Get subscriber
     *
     * @return string
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }

    /**
     * Set shop
     *
     * @param \AppBundle\Entity\Shop $shop
     *
     * @return PhoneShop
     */
    public function setShop(\AppBundle\Entity\Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \AppBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }
}
