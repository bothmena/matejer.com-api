<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\AppEvents;
use AppBundle\Entity\User;
use AppBundle\Event\UserConfirmEmailSuccessEvent;
use AppBundle\Event\UserRegistrationSuccessEvent;
use AppBundle\Form\User\ResetPasswordType;
use AppBundle\Form\User\UserEditType;
use AppBundle\Form\User\UserType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends FOSRestController {

    /**
     * @Rest\Get(path="/users/{id}", requirements={"id": "\d+"}, name="api_user_read", options={ "method_prefix" =
     *                               false })
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     * @return Response
     */
    public function getAction( int $id ) {

        $user = $this->getDoctrine()->getRepository( 'AppBundle:User' )->find( $id );
        if ( !$user instanceof User ) {
            throw $this->createNotFoundException( 'User not found.' );
        }

        $view = $this->view( $user, 200 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/users/{username}", name="api_user_read_username", options={ "method_prefix" = false })
     * @Rest\View()
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $username
     * @return Response
     */
    public function getByUsernameAction( string $username ) {

        $user = $this->getDoctrine()->getRepository( 'AppBundle:User' )
            ->findOneBy( ['username' => $username] );

        if ( !$user instanceof User ) {
            throw $this->createNotFoundException( 'User not found' );
        }

        $view = $this->view( $user, 200 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/users/t/{token}", name="api_user_read_token", options={ "method_prefix" = false })
     * @Rest\View()
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $token
     * @return Response
     */
    public function existByTokenAction( Request $request, string $token ) {

        $criteria = [ 'confirmationToken' => $token ];
        $enabled = $request->query->get( 'enabled' );
        if ( $enabled === 'true' ) {

            $criteria[ 'enabled' ] = true;
        } else if ( $enabled === 'false' ) {

            $criteria[ 'enabled' ] = false;
        }

        $user = $this->getDoctrine()->getRepository( 'AppBundle:User' )->findOneBy( $criteria );

        return $this->handleView( $this->view( [ 'exist' => $user !== NULL ], 200 ) );
    }

    /**
     * @Rest\Get(path="/users", name="api_user_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @return Response
     */
    public function cgetAction( Request $request ) {

        $fields = [
            'id'         => 'id',
            'username'   => 'username',
            'first_name' => 'firstName',
            'last_name'  => 'lastName',
            'email'      => 'email',
            'last_login' => 'lastLogin',
        ];
        $roles = [
            'professor' => 'ROLE_PROFESSOR',
            'ins_admin' => 'ROLE_INS_ADMIN',
            'ins_staff' => 'ROLE_INS_STAFF',
            'student'   => 'ROLE_STUDENT',
        ];

        $role = $request->query->get( 'role' );
        $orderBy = $request->query->get( 'order_by' );
        $gender = $request->query->get( 'gender' );
        $orderBy = array_key_exists( $orderBy, $fields ) ? $fields[ $orderBy ] : NULL;
        $role = array_key_exists( $role, $roles ) ? $roles[ $role ] : NULL;

        $users = $this->getDoctrine()->getRepository( 'AppBundle:User' )
            ->getFiltredUsers( $role, $gender, $orderBy );

        return $this->handleView( $this->view( $users, 200 ) );
    }

    /**
     * @Rest\Post(path="/users", name="api_user_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @return Response
     */
    public function newAction( Request $request ) {

        $userManager = $this->get( 'fos_user.user_manager' );
        $dispatcher = $this->get( 'event_dispatcher' );

        $user = $userManager->createUser();
        $form = $this->createForm( UserType::class, $user );
        $data = json_decode( $request->getContent(), true );
        $form->submit( $data );

        if ( $form->isValid() ) {

            $event = new UserRegistrationSuccessEvent( $user );
            $dispatcher->dispatch( AppEvents::REGISTRATION_SUCCESS, $event );
            $view = $this->view( NULL, 201 );

            $response = $this->handleView( $view );
            $response->headers->set( 'Location', $this->generateUrl( 'api_user_read', [
                'version' => 'v1',
                'id'      => $user->getId(),
            ] ) );

            return $response;
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Route(path="/users/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_user_update",
     *     options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction( Request $request, int $id ) {

        if ( !$this->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {

            return new JsonResponse([
                'status_code' => 401,
                'error_code' => 'not_authenticated',
                'message' => 'you are not authenticated',
            ], 401);
        }
        $user = $this->getUser();

        if ( !$user instanceof User ) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'user was not found',
            ], 404);
        }

        if ( $user->getId() !== $id ) {

            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_user_property',
                'message' => 'you are not allowed to do this action',
            ], 403);
        }

        return $this->processForm( $request, $user );
    }

    /**
     * @Rest\Get(path="/users/confirm-email/{token}", name="api_user_confirm_email", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function confirmEmailAction( $token ) {

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get( 'fos_user.user_manager' );
        $user = $userManager->findUserByConfirmationToken( $token );

        if ( NULL === $user ) {
            throw new NotFoundHttpException(
                sprintf( 'The user with confirmation token "%s" does not exist', $token )
            );
        }

        /** @var $dispatcher TraceableEventDispatcherInterface */
        $dispatcher = $this->get( 'event_dispatcher' );

        $event = new UserConfirmEmailSuccessEvent( $user );
        $dispatcher->dispatch( AppEvents::USER_CONFIRM_EMAIL_SUCCESS, $event );

        if ( NULL === $response = $event->getResponse() ) {
            return new JsonResponse( [], 204 );
        }

        return $response;
    }

    /**
     * @Rest\Post(path="/users/resend-code/{email}", name="api_user_resend_code", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function resendCodeAction( $email ) {

        /**
         * @var $userManager \FOS\UserBundle\Model\UserManagerInterface
         */
        $userManager = $this->get( 'fos_user.user_manager' );
        $user = $userManager->findUserByEmail( $email );

        if ( NULL === $user ) {
            throw new NotFoundHttpException(
                sprintf( 'The user with email "%s" does not exist', $email )
            );
        }

        /**
         * @todo: send an email with the confirmation email code.
         */

        return new JsonResponse( NULL, 204 );
    }

    /**
     * @Rest\Post(path="/users/{id}/reset-password", name="api_user_reset_pass_code",
     *     options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function resetPasswordAction( Request $request, int $id ) {

        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(['id' => $id]);
        if ( !$user ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user',
                'message'     => 'User was not found',
            ], 404 );
        }
        $form = $this->createForm( ResetPasswordType::class, $user );
        $data = json_decode( $request->getContent(), true );
        $form->submit( $data );

        if ( $form->isValid() ) {

            $newPass = $form->get('plainPassword')->get('password')->getData();
            $user->setPlainPassword( $newPass );
            $userManager->updatePassword( $user );
            $userManager->updateUser( $user );

            return $this->handleView( $this->view( NULL, 204 ) );
        }

        return $this->handleView( $this->view( $form, 400 ) );
    }

    private function processForm( Request $request, User $user ) {

        $form = $this->createForm( UserEditType::class, $user );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $user );
            $em->flush();

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}
