<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\Feature;
use AppBundle\Form\Type\FeatureType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FeatureController extends FOSRestController {

    private $entityFields = ['category', 'image', 'name', 'description', 'locale'];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/products/{productId}/features/{id}", requirements={"productId": "\d+", "id": "\d+"},
     *     name="api_feature_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Feature",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $productId, int $id ) {

        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )
            ->find( $productId );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product',
                'message'     => 'Product was not found',
            ], 404 );
        }

        $feature = $this->getDoctrine()->getRepository( 'AppBundle:Feature' )
            ->getProductFeature( $productId, $id );
        if ( !$feature ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_feature',
                'message'     => 'Product feature was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $feature->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $feature, 200 ) );
    }

    /**
     * @Rest\Get(path="/products/{productId}/features", requirements={"productId": "\d+"},
     *     name="api_feature_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Feature",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @return Response
     */
    public function cgetAction( Request $request, int $productId ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )->find( $productId );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product',
                'message'     => 'Product was not found',
            ], 404 );
        }

        $features = $this->getDoctrine()->getRepository( 'AppBundle:Feature' )
            ->getProductFeatures( $productId );

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $features as $feature ) {

                $result[ $feature->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $feature->getId() ][ $field ] = $feature->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $features, 200 ) );
    }

    /**
     * @Rest\Post(path="/products/{productId}/features", requirements={"productId": "\d+"},
     *     name="api_feature_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Feature",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @return Response
     */
    public function newAction( Request $request, int $productId ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $feature = new Feature();
        $feature->setProduct( $result->getProduct() );

        return $this->processForm( $request, $feature, true );
    }

    /**
     * @Rest\Route(path="/products/{productId}/features/{id}", requirements={"productId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_feature_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Feature",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Features"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $productId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $feature = $this->getDoctrine()->getRepository( 'AppBundle:Feature' )
            ->getProductFeature( $productId, $id );
        if ( !$feature ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_feature',
                'message'     => 'Product feature was not found',
            ], 404 );
        }

        return $this->processForm( $request, $feature, false );
    }

    /**
     * @Rest\Delete(path="/products/{productId}/features/{id}", requirements={"productId": "\d+","id": "\d+"},
     *     name="api_feature_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Feature",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Features"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $productId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $feature = $this->getDoctrine()->getRepository( 'AppBundle:Feature' )
            ->getProductFeature( $productId, $id );
        if ( !$feature ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_feature',
                'message'     => 'Product feature was not found',
            ], 404 );
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove( $feature );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Feature $feature
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Feature $feature, bool $isNew = false ) {

        $form = $this->createForm( FeatureType::class, $feature );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $feature );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_feature_read', [
                    'version' => 'v1',
                    'productId'   => $feature->getProduct()->getId(),
                    'id'      => $feature->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }}
