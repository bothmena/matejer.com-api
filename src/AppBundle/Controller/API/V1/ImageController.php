<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 10/12/17
 * Time: 14:00
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\AppEvents;
use AppBundle\Entity\Image;
use AppBundle\Event\ProductImageUploadSuccessEvent;
use AppBundle\Event\ShopImageUploadSuccessEvent;
use AppBundle\Event\UserImageUploadSuccessEvent;
use AppBundle\Form\Image\ImageEntityType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends FOSRestController {

    /**
     * @Rest\Get(path="/images/{id}", requirements={"id": "\d+"}, name="api_image_read", options={
     *     "method_prefix" = false })
     * @ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     *
     * @return Response
     */
    public function getAction( int $id ) {

        $image = $this->getDoctrine()->getRepository( 'AppBundle:Image' )->find( $id );

        if ( !$image ) {
            return new JsonResponse( [ 'code' => 404, 'error' => 'Image was not found' ], 404 );
        }

        return $this->handleView( $this->view( $image, 200 ) );
    }

    /**
     * @Rest\Get(path="/images", name="api_image_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     *
     * @return Response
     */
    public function cgetAction() {

        $images = $this->getDoctrine()->getRepository( 'AppBundle:Image' )->findAll();

        return $this->handleView( $this->view( $images, 200 ) );
    }

    /**
     * @Rest\Post(path="/images/{entity}/{id}/{type}", name="api_image_create", requirements={ "entity": "user|shop|product",
     *         "id": "\d+","type": "profile|cover|gallery" }, options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $entity
     * @param int $id
     * @param string $type
     * @return mixed|JsonResponse|Response
     */
    public function newAction( string $entity, int $id, string $type ) {

        try {

            $image = $this->get( 'abo.img_upload' )->uploadImage( $entity, $type );

            return $this->processEvent( $image, $id );
        } catch ( FileException $fileException ) {
            return new JsonResponse( [
                'message' => 'we couldn\'t process the image upload.',
                'error'   => $fileException->getMessage(),
            ], 500 );
        }
    }

    /**
     * @Rest\Route(path="/images/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"},
     *     name="api_image_update", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $id
     *
     * @return Response
     */
    public function editAction( Request $request, int $id ) {

        $image = $this->getDoctrine()->getRepository( 'AppBundle:Image' )->find( $id );

        if ( !$image ) {
            return new JsonResponse( [ 'code' => 404, 'error' => 'Image was not found' ], 404 );
        }

        return $this->processForm( $request, $image );
    }

    /**
     * @Rest\Delete(path="/images/{id}", requirements={"id": "\d+"}, name="api_image_delete", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     *
     * @return Response
     * @internal param Request $request
     *
     */
    public function removeAction( int $id ) {

        $image = $this->getDoctrine()->getRepository( 'AppBundle:Image' )->find( $id );

        if ( $image ) {

            $em = $this->getDoctrine()->getManager();
            $em->remove( $image );
            $em->flush();
            $path = $this->get( 'kernel' )->getRootDir() . '/../web' . $image->getSource();
            unlink( $path );
        } else {
            return new JsonResponse( [ 'code' => 404, 'error' => 'Image was not found' ], 404 );
        }

        return new Response( NULL, 204 );
    }

    private function processEvent( Image $image, int $id ) {

        $dispatcher = $this->get( 'event_dispatcher' );
        switch ( $image->getEntity() ) {

            case 'shop':
                $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )->find( $id );
                if ( $shop ){

                    $event = new ShopImageUploadSuccessEvent( $shop, $image );
                    $dispatcher->dispatch( AppEvents::SHOP_IMAGE_UPLOAD_SUCCESS, $event );

                    return $event->getResponse();
                }
                break;
            case 'product':
                $product = $this->getDoctrine()->getRepository( 'AppBundle:CategoryProduct' )->find( $id );
                if ( $product ) {

                    $event = new ProductImageUploadSuccessEvent( $product, $image );
                    $dispatcher->dispatch( AppEvents::PRODUCT_IMAGE_UPLOAD_SUCCESS, $event );

                    return $event->getResponse();
                }
                break;
            case 'user':
                $user = $this->getDoctrine()->getRepository( 'AppBundle:User' )->find( $id );
                if ( $user ) {

                    $event = new UserImageUploadSuccessEvent( $user, $image );
                    $dispatcher->dispatch( AppEvents::USER_IMAGE_UPLOAD_SUCCESS, $event );

                    return $event->getResponse();
                }
                break;

            default:
                return new Response( 'Entity not valid: ' . $image->getEntity(), 400 );
        }

        return new Response( 'Entity not found: ' . $image->getEntity(), 404 );
    }

    /**
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    private function processForm( Request $request, Image $image ) {

        $form = $this->createForm( ImageEntityType::class, $image );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $image );
            $em->flush();

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}
