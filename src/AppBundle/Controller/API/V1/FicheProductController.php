<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\FicheProduct;
use AppBundle\Form\Type\FicheProductType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FicheProductController extends FOSRestController {

    private $entityFields = ['category_procuct', 'name', 'group', 'value', 'type', 'language'];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/products/{productId}/ficheproducts/{id}", requirements={"productId": "\d+", "id": "\d+"},
     *     name="api_ficheproduct_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="FicheProduct",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $productId, int $id ) {

        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )
            ->find( $productId );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product',
                'message'     => 'Product was not found',
            ], 404 );
        }

        $ficheproduct = $this->getDoctrine()->getRepository( 'AppBundle:FicheProduct' )
            ->getProductFicheProduct( $productId, $id );
        if ( !$ficheproduct ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_ficheproduct',
                'message'     => 'Product ficheproduct was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $ficheproduct->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $ficheproduct, 200 ) );
    }

    /**
     * @Rest\Get(path="/products/{productId}/ficheproducts", requirements={"productId": "\d+"},
     *     name="api_ficheproduct_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="FicheProduct",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @return Response
     */
    public function cgetAction( Request $request, int $productId ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )->find( $productId );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product',
                'message'     => 'Product was not found',
            ], 404 );
        }

        $ficheproducts = $this->getDoctrine()->getRepository( 'AppBundle:FicheProduct' )
            ->getProductFicheProducts( $productId );

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $ficheproducts as $ficheproduct ) {

                $result[ $ficheproduct->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $ficheproduct->getId() ][ $field ] = $ficheproduct->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $ficheproducts, 200 ) );
    }

    /**
     * @Rest\Post(path="/products/{productId}/ficheproducts", requirements={"productId": "\d+"},
     *     name="api_ficheproduct_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="FicheProduct",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @return Response
     */
    public function newAction( Request $request, int $productId ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $ficheproduct = new FicheProduct();
        $ficheproduct->setProduct( $result->getProduct() );

        return $this->processForm( $request, $ficheproduct, true );
    }

    /**
     * @Rest\Route(path="/products/{productId}/ficheproducts/{id}", requirements={"productId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_ficheproduct_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="FicheProduct",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "FicheProducts"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $productId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $ficheproduct = $this->getDoctrine()->getRepository( 'AppBundle:FicheProduct' )
            ->getProductFicheProduct( $productId, $id );
        if ( !$ficheproduct ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_ficheproduct',
                'message'     => 'Product ficheproduct was not found',
            ], 404 );
        }

        return $this->processForm( $request, $ficheproduct, false );
    }

    /**
     * @Rest\Delete(path="/products/{productId}/ficheproducts/{id}", requirements={"productId": "\d+","id": "\d+"},
     *     name="api_ficheproduct_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="FicheProduct",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "FicheProducts"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $productId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $ficheproduct = $this->getDoctrine()->getRepository( 'AppBundle:FicheProduct' )
            ->getProductFicheProduct( $productId, $id );
        if ( !$ficheproduct ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_ficheproduct',
                'message'     => 'Product ficheproduct was not found',
            ], 404 );
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove( $ficheproduct );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param FicheProduct $ficheproduct
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, FicheProduct $ficheproduct, bool $isNew = false ) {

        $form = $this->createForm( FicheProductType::class, $ficheproduct );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $ficheproduct );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_ficheproduct_read', [
                    'version' => 'v1',
                    'productId'   => $ficheproduct->getProduct()->getId(),
                    'id'      => $ficheproduct->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }}
