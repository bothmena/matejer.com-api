<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\Collection;
use AppBundle\Entity\CollectionProduct;
use AppBundle\Entity\ProductShop;
use AppBundle\Form\Shop\ProductShopType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductShopController extends FOSRestController
{

    private $entityFields = ['category_product', 'reference', 'price', 'discount', 'warranty', 'availability'];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/shops/{shopId}/products/{id}", requirements={"shopId": "\d+", "id": "\d+"},
     *     name="api_product_shop_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="ProductShop",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @param int $id
     * @return Response
     */
    public function getAction(Request $request, int $shopId, int $id)
    {

        $shop = $this->getDoctrine()->getRepository('AppBundle:Shop')
            ->find($shopId);
        if (!$shop) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.shop',
                'message' => 'Shop was not found',
            ], 404);
        }

        $productShop = $this->getDoctrine()->getRepository('AppBundle:ProductShop')
            ->getShopProductShop($shopId, $id);
        if (!$productShop) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.shop_product_shop',
                'message' => 'Shop product_shop was not found',
            ], 404);
        }

        $fields = $request->query->get('fields');
        if ($fields) {

            $result = [];
            foreach ($fields as $field) {

                if (in_array($field, $this->entityFields)) {
                    $result[$field] = $productShop->getField($field);
                }
            }

            return $this->handleView($this->view($result, 200));
        }

        return $this->handleView($this->view($productShop, 200));
    }

    /**
     * @Rest\Get(path="/shops/{shopId}/products", requirements={"shopId": "\d+"},
     *     name="api_product_shop_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="ProductShop",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @return Response
     */
    public function cgetAction(Request $request, int $shopId)
    {

        $limit = $request->query->getInt('limit', 20);
        $shop = $this->getDoctrine()->getRepository('AppBundle:Shop')->find($shopId);
        if (!$shop) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.shop',
                'message' => 'Shop was not found',
            ], 404);
        }

        $productShops = $this->getDoctrine()->getRepository('AppBundle:ProductShop')
            ->getShopProductShops($shopId);

        $fields = $request->query->get('fields');
        if ($fields) {

            $result = [];
            foreach ($productShops as $productShop) {

                $result[$productShop->getId()] = [];
                foreach ($fields as $field) {

                    if (in_array($field, $this->entityFields)) {
                        $result[$productShop->getId()][$field] = $productShop->getField($field);
                    }
                }
            }

            return $this->handleView($this->view($result, 200));
        }

        return $this->handleView($this->view($productShops, 200));
    }

    /**
     * @Rest\Get(path="/shops/{shopSlug}/products", requirements={"shopId": "\d+"},
     *     name="api_product_shop_read_all_slug", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="ProductShop",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $shopSlug
     * @return Response
     */
    public function cgetByShopSlugAction(Request $request, string $shopSlug)
    {

        $limit = $request->query->getInt('limit', 20);
        $shop = $this->getDoctrine()->getRepository('AppBundle:Shop')->findOneBy(['slug' => $shopSlug]);
        if (!$shop) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.shop',
                'message' => 'Shop was not found',
            ], 404);
        }

        $productShops = $this->getDoctrine()->getRepository('AppBundle:ProductShop')
            ->getShopProductsByShopSlug($shopSlug);

        $fields = $request->query->get('fields');
        if ($fields) {

            $result = [];
            foreach ($productShops as $productShop) {

                $result[$productShop->getId()] = [];
                foreach ($fields as $field) {

                    if (in_array($field, $this->entityFields)) {
                        $result[$productShop->getId()][$field] = $productShop->getField($field);
                    }
                }
            }

            return $this->handleView($this->view($result, 200));
        }

        return $this->handleView($this->view($productShops, 200));
    }

    /**
     * @Rest\Post(path="/shops/{shopId}/products", requirements={"shopId": "\d+"},
     *     name="api_product_shop_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="ProductShop",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @return Response
     */
    public function newAction(Request $request, int $shopId)
    {

        $result = $this->get('abo.access_checker')->checkForShop($shopId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $productShop = new ProductShop();
        $productShop->setShop($result->getShop());

        return $this->processForm($request, $productShop, true);
    }

    /**
     * @Rest\Route(path="/shops/{shopId}/products/{id}", requirements={"shopId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_product_shop_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="ProductShop",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "ProductShops"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, int $shopId, int $id)
    {

        $result = $this->get('abo.access_checker')->checkForUser('ROLE_USER');
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $productShop = $this->getDoctrine()->getRepository('AppBundle:ProductShop')
            ->getShopProductShop($shopId, $id);
        if (!$productShop) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.shop_product_shop',
                'message' => 'Shop product_shop was not found',
            ], 404);
        }

        return $this->processForm($request, $productShop, false);
    }

    /**
     * @Rest\Delete(path="/shops/{shopId}/products/{id}", requirements={"shopId": "\d+","id": "\d+"},
     *     name="api_product_shop_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="ProductShop",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "ProductShops"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $shopId
     * @param int $id
     * @return Response
     */
    public function deleteAction(int $shopId, int $id)
    {

        $result = $this->get('abo.access_checker')->checkForUser('ROLE_USER');
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $productShop = $this->getDoctrine()->getRepository('AppBundle:ProductShop')
            ->getShopProductShop($shopId, $id);
        if (!$productShop) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.shop_product_shop',
                'message' => 'Shop product_shop was not found',
            ], 404);
        }
        $em = $this->getDoctrine()->getManager();

        $prodCols = $this->getDoctrine()->getRepository('AppBundle:CollectionProduct')
            ->findBy(['productShop' => $productShop]);
        $this->removeOldCollections($prodCols, []);
        /**
         * @todo remove payment methods also.
         */

        $em->remove($productShop);
        $em->flush();

        return $this->handleView($this->view(NULL, 204));
    }

    /**
     * @param Request $request
     * @param ProductShop $productShop
     * @param bool $isNew
     * @return Response
     */
    private function processForm(Request $request, ProductShop $productShop, bool $isNew = false)
    {

        $form = $this->createForm(ProductShopType::class, $productShop);

        $data = json_decode($request->getContent(), true);
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($productShop);

            $prodCols = $this->getDoctrine()->getRepository('AppBundle:CollectionProduct')
                ->findBy(['productShop' => $productShop]);
            $oldCols = $this->cPToC($prodCols);

            /** @var Collection[] $newCols */
            $newCols = $form->get('collection')->getData()->toArray();
            $this->addNewCollections($oldCols, $newCols, $productShop);
            $this->removeOldCollections($prodCols, $newCols);
            /**
             * @todo add/remove payment methods also.
             */

            $em->flush();

            if ($isNew) {

                $view = $this->view(NULL, 201);

                $response = $this->handleView($view);
                $response->headers->set('Location', $this->generateUrl('api_product_shop_read', [
                    'version' => 'v1',
                    'shopId' => $productShop->getShop()->getId(),
                    'id' => $productShop->getId(),
                ]));

                return $response;
            }

            $view = $this->view(NULL, 204);

            return $this->handleView($view);
        }

        $view = $this->view($form, 400);

        return $this->handleView($view);
    }

    /**
     * @param CollectionProduct[] $collectionProducts
     * @return array
     */
    private function cPToC($collectionProducts)
    {

        $res = [];
        foreach ($collectionProducts as $collectionProduct) {
            array_push($res, $collectionProduct->getCollection());
        }

        return $res;
    }

    private function addNewCollections(array $oldCols, array $newCols, ProductShop $productShop)
    {

        foreach ($newCols as $collection) {

            if (!in_array($collection, $oldCols)) {
                $colProd = new CollectionProduct($collection, $productShop);
                $collection->setProductNb($collection->getProductNb() + 1);
                $this->getDoctrine()->getManager()->persist($colProd);
            }
        }
    }

    /**
     * @param CollectionProduct[] $oldCols
     * @param Collection[] $newCols
     */
    private function removeOldCollections(array $oldCols, array $newCols)
    {

        foreach ($oldCols as $oldCol) {

            if (!in_array($oldCol->getCollection(), $newCols)) {
                $oldCol->getCollection()->setProductNb($oldCol->getCollection()->getProductNb() - 1);
                $this->getDoctrine()->getManager()->remove($oldCol);
            }
        }
    }

    /**
     * @todo define addNewPayments & removeOldPayments
     */
}
