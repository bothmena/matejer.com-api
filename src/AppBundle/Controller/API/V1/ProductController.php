<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\Product;
use AppBundle\Form\Product\ProductType;
use AppBundle\Utils\AccessCheckerResult;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends FOSRestController {

    private $entityFields = ['reference', 'name', 'mark', 'public', 'date', 'favored_nb'];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/products/{id}", requirements={"id": "\d+"},
     *     name="api_product_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single product with all the fields unless fields parameter is provided.",
     *     section="Product",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Product id"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired product fields, select from: reference, name, mark, public, date, favored_nb, default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "products", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $id ) {

        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )
            ->find( $id );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product',
                'message'     => 'Product was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $product->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $product, 200 ) );
    }

    /**
     * @Rest\Get(path="/products/{slug}", name="api_product_read_by_slug", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single product with all the fields unless fields parameter is provided.",
     *     section="Product",
     *     requirements={
     *         {"name"="slug","dataType"="string","description"="Product slug"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired product fields, select from: reference, name, mark, public, date, favored_nb, default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "products", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function getBySlugAction( Request $request, string $slug ) {

        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )
            ->findOneBy( [ 'slug' => $slug ] );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product',
                'message'     => 'Product was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $product->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $product, 200 ) );
    }

    /**
     * @Rest\Get(path="/products", name="api_product_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Product",
     *     description="Retrieve paginated list of products with all the fields unless fields parameter is provided.",
     *     parameters={
     *         {"name"="limit", "dataType"="integer", "required"=false, "description"="max products in response, default 20"},
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired product fields, select from: reference, name, mark, public, date, favored_nb, default: all"},
     *         {"name"="page", "dataType"="integer", "required"=false, "description"="the page number, default: 1"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *     },
     *     views={"default", "v1", "version1", "products", "cget", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @return Response
     */
    public function cgetAction( Request $request ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $query = $this->getDoctrine()->getRepository( 'AppBundle:Product' )->getAllProductsQuery();

        /**
         * @var Product[] $products
         */
        $products = $this->get('knp_paginator')
            ->paginate( $query, $request->query->getInt('page', 1), $limit )->getItems();
        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $products as $product ) {

                $result[ $product->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $product->getId() ][ $field ] = $product->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $products, 200 ) );
    }

    /**
     * @Rest\Post(path="/products", name="api_product_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Product",
     *     description="Create a new Product Object",
     *     input="AppBundle\Form\ProductType",
     *     output="AppBundle\Entity\Product",
     *     statusCodes={
     *         201="Created: The request has been fulfilled and resulted in a new resource being created.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "products", "new", "post-method"},
     *     tags={"v1" = "#48A01D", "POST" = "#1D3BA0"},
     * )
     * @param Request $request
     * @return Response
     */
    public function newAction( Request $request ) {

        /**
         * @var $result AccessCheckerResult
         */
        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_TAJER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $product = new Product();

        return $this->processForm( $request, $product, true );
    }

    /**
     * @Rest\Route(path="/products/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"},
     *     name="api_product_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Product",
     *     description="Update product details",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Product id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being modified.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "products", "edit", "patch-method", "put-method"},
     *     tags={"v1" = "#48A01D", "PUT" = "#381DA0", "PATCH" = "#1D85A0"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $id ) {

        /**
         * @var $result AccessCheckerResult
         */
        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_TAJER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        /**
         * @var $product Product
         */
        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )->find( $id );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_product',
                'message'     => 'Institute product was not found',
            ], 404 );
        }

        return $this->processForm( $request, $product, false );
    }

    /**
     * @Rest\Delete(path="/products/{id}", requirements={"id": "\d+"},
     *     name="api_product_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Product",
     *     description="Delete a single product",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Product id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being deleted.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "products", "delete", "delete-method"},
     *     tags={"v1" = "#48A01D", "DELETE" = "#A01D27"},
     * )
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $id ) {

        /**
         * @var $result AccessCheckerResult
         */
        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_TAJER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )->find( $id );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product',
                'message'     => 'Institute product was not found',
            ], 404 );
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove( $product );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Product $product
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Product $product, bool $isNew = false ) {

        $form = $this->createForm( ProductType::class, $product );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $product );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_product_read', [
                    'version' => 'v1',
                    'id'      => $product->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }}
