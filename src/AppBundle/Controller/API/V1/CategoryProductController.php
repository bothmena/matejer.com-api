<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\CategoryProduct;
use AppBundle\Form\Product\CategoryProductType;
use AppBundle\Form\Product\CategoryProductEditType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryProductController extends FOSRestController {

    private $entityFields = ['product', 'category', 'general_spec', 'rate_stat', 'image'];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/categoryproducts/{id}", requirements={"id": "\d+"},
     *     name="api_categoryproduct_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single categoryproduct with all the fields unless fields parameter is provided.",
     *     section="CategoryProduct",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="CategoryProduct id"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired categoryproduct fields, select from: product, category, general_spec, rate_stat, image, default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "categoryproducts", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $id ) {

        $categoryProduct = $this->getDoctrine()->getRepository( 'AppBundle:CategoryProduct' )
            ->find( $id );
        if ( !$categoryProduct ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.categoryproduct',
                'message'     => 'CategoryProduct was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $categoryProduct->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $categoryProduct, 200 ) );
    }

    /**
     * @Rest\Get(path="/categoryproducts/{slug}", name="api_categoryproduct_read_by_slug", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single categoryproduct with all the fields unless fields parameter is provided.",
     *     section="CategoryProduct",
     *     requirements={
     *         {"name"="slug","dataType"="string","description"="CategoryProduct slug"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired categoryproduct fields, select from: product, category, general_spec, rate_stat, image, default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "categoryproducts", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function getBySlugAction( Request $request, string $slug ) {

        $categoryProduct = $this->getDoctrine()->getRepository( 'AppBundle:CategoryProduct' )
            ->getCatProductBySlug( $slug );
        if ( !$categoryProduct ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.categoryproduct',
                'message'     => 'CategoryProduct was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $categoryProduct->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $categoryProduct, 200 ) );
    }

    /**
     * @Rest\Get(path="/categoryproducts", name="api_categoryproduct_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="CategoryProduct",
     *     description="Retrieve paginated list of categoryproducts with all the fields unless fields parameter is provided.",
     *     parameters={
     *         {"name"="limit", "dataType"="integer", "required"=false, "description"="max categoryproducts in response, default 20"},
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired categoryproduct fields, select from: product, category, general_spec, rate_stat, image, default: all"},
     *         {"name"="page", "dataType"="integer", "required"=false, "description"="the page number, default: 1"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *     },
     *     views={"default", "v1", "version1", "categoryproducts", "cget", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @return Response
     */
    public function cgetAction( Request $request ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $categoryProducts = $this->getDoctrine()->getRepository( 'AppBundle:CategoryProduct' )
            ->findAll();

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $categoryProducts as $categoryProduct ) {

                $result[ $categoryProduct->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $categoryProduct->getId() ][ $field ] = $categoryProduct->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $categoryProducts, 200 ) );
    }

    /**
     * @Rest\Post(path="/categoryproducts", name="api_categoryproduct_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="CategoryProduct",
     *     description="Create a new CategoryProduct Object",
     *     input="AppBundle\Form\CategoryProductType",
     *     output="AppBundle\Entity\CategoryProduct",
     *     statusCodes={
     *         201="Created: The request has been fulfilled and resulted in a new resource being created.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "categoryproducts", "new", "post-method"},
     *     tags={"v1" = "#48A01D", "POST" = "#1D3BA0"},
     * )
     * @param Request $request
     * @return Response
     */
    public function newAction( Request $request ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_TAJER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $categoryProduct = new CategoryProduct();

        return $this->processForm( $request, $categoryProduct, true );
    }

    /**
     * @Rest\Route(path="/categoryproducts/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"},
     *     name="api_categoryproduct_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="CategoryProduct",
     *     description="Update categoryproduct details",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="CategoryProduct id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being modified.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "categoryproducts", "edit", "patch-method", "put-method"},
     *     tags={"v1" = "#48A01D", "PUT" = "#381DA0", "PATCH" = "#1D85A0"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function uploadImagesAction( Request $request, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_TAJER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $categoryProduct = $this->getDoctrine()->getRepository( 'AppBundle:CategoryProduct' )->find( $id );
        if ( !$categoryProduct ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_category_product',
                'message'     => 'Institute category product was not found',
            ], 404 );
        }

        return $this->processForm( $request, $categoryProduct );
    }

    /**
     * @Rest\Route(path="/categoryproducts/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"},
     *     name="api_categoryproduct_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="CategoryProduct",
     *     description="Update categoryproduct details",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="CategoryProduct id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being modified.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "categoryproducts", "edit", "patch-method", "put-method"},
     *     tags={"v1" = "#48A01D", "PUT" = "#381DA0", "PATCH" = "#1D85A0"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_TAJER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $categoryProduct = $this->getDoctrine()->getRepository( 'AppBundle:CategoryProduct' )->find( $id );
        if ( !$categoryProduct ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_category_product',
                'message'     => 'Institute category product was not found',
            ], 404 );
        }

        return $this->processForm( $request, $categoryProduct );
    }

    /**
     * @Rest\Delete(path="/categoryproducts/{id}", requirements={"id": "\d+"},
     *     name="api_categoryproduct_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="CategoryProduct",
     *     description="Delete a single categoryproduct",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="CategoryProduct id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being deleted.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "categoryproducts", "delete", "delete-method"},
     *     tags={"v1" = "#48A01D", "DELETE" = "#A01D27"},
     * )
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_TAJER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $categoryProduct = $this->getDoctrine()->getRepository( 'AppBundle:CategoryProduct' )->find( $id );
        if ( !$categoryProduct ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.categoryproduct',
                'message'     => 'Institute categoryproduct was not found',
            ], 404 );
        }
        $em = $this->getDoctrine()->getManager();

        $images = $em->getRepository('AppBundle:ImageProduct')->findBy(['categoryProduct' => $categoryProduct]);
        foreach ( $images as $image ) {
            $em->remove($image);
        }
        $offers = $em->getRepository('AppBundle:ProductShop')->findBy(['categoryProduct' => $categoryProduct]);
        foreach ( $offers as $offer ) {
            $em->remove($offer);
        }
        $features = $em->getRepository('AppBundle:FeatureProduct')->findBy(['categoryProduct' => $categoryProduct]);
        foreach ( $features as $feature ) {
            $em->remove($feature);
        }
        // FicheProduct, TagProducts, ProductSize, ProductColor

        $em->remove( $categoryProduct );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param CategoryProduct $categoryProduct
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, CategoryProduct $categoryProduct, bool $isNew = false ) {

        if ( $isNew )
            $form = $this->createForm( CategoryProductType::class, $categoryProduct );
        else
            $form = $this->createForm( CategoryProductEditType::class, $categoryProduct );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $categoryProduct );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_categoryproduct_read', [
                    'version' => 'v1',
                    'id'      => $categoryProduct->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }}
