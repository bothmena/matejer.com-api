<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\Shop;
use AppBundle\Form\Shop\ShopType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ShopController extends FOSRestController {

    private $entityFields = ['location', 'name', 'currency', 'description', 'website', 'slogan'];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/shops/{id}", requirements={"id": "\d+"},
     *     name="api_shop_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single shop with all the fields unless fields parameter is provided.",
     *     section="Shop",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Shop id"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired shop fields, select from: location, name, currency, description, website, slogan, default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "shops", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $id ) {

        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )
            ->find( $id );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop',
                'message'     => 'Shop was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $shop->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $shop, 200 ) );
    }

    /**
     * @Rest\Get(path="/shops/{slug}", name="api_shop_read_by_slug", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Retrieve a single shop with all the fields unless fields parameter is provided.",
     *     section="Shop",
     *     requirements={
     *         {"name"="slug","dataType"="string","description"="Shop slug"}
     *     },
     *     parameters={
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired shop fields, select from: location, name, currency, description, website, slogan, default: all"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "shops", "get", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function getBySlugAction( Request $request, string $slug ) {

        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )
            ->findOneBy( [ 'slug' => $slug ] );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop',
                'message'     => 'Shop was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $shop->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $shop, 200 ) );
    }

    /**
     * @Rest\Get(path="/shops", name="api_shop_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Shop",
     *     description="Retrieve paginated list of shops with all the fields unless fields parameter is provided.",
     *     parameters={
     *         {"name"="limit", "dataType"="integer", "required"=false, "description"="max shops in response, default 20"},
     *         {"name"="fields", "dataType"="array", "required"=false, "description"="the desired shop fields, select from: location, name, currency, description, website, slogan, default: all"},
     *         {"name"="page", "dataType"="integer", "required"=false, "description"="the page number, default: 1"},
     *     },
     *     statusCodes={
     *         200="OK: The request has succeeded.",
     *     },
     *     views={"default", "v1", "version1", "shops", "cget", "get-method"},
     *     tags={"v1" = "#48A01D", "GET" = "#1DA034"},
     * )
     * @param Request $request
     * @return Response
     */
    public function cgetAction( Request $request ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $query = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )->getAllShopsQuery();
        /**
         * @var Shop[] $shops
         */
        $shops = $this->get('knp_paginator')
            ->paginate( $query, $request->query->getInt('page', 1), $limit )->getItems();

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $shops as $shop ) {

                $result[ $shop->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $shop->getId() ][ $field ] = $shop->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $shops, 200 ) );
    }

    /**
     * @Rest\Post(path="/shops", name="api_shop_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Shop",
     *     description="Create a new Shop Object",
     *     input="AppBundle\Form\Shop\ShopType",
     *     output="AppBundle\Entity\Shop",
     *     statusCodes={
     *         201="Created: The request has been fulfilled and resulted in a new resource being created.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "shops", "new", "post-method"},
     *     tags={"v1" = "#48A01D", "POST" = "#1D3BA0"},
     * )
     * @param Request $request
     * @return Response
     */
    public function newAction( Request $request ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $shop = new Shop();

        return $this->processForm( $request, $shop, true );
    }

    /**
     * @Rest\Route(path="/shops/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"},
     *     name="api_shop_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Shop",
     *     description="Update shop details",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Shop id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being modified.",
     *         400="Bad Request: At least one field of the form was not valid.",
     *     },
     *     views={"default", "v1", "version1", "shops", "edit", "patch-method", "put-method"},
     *     tags={"v1" = "#48A01D", "PUT" = "#381DA0", "PATCH" = "#1D85A0"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )->find( $id );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_shop',
                'message'     => 'Institute shop was not found',
            ], 404 );
        }

        return $this->processForm( $request, $shop, false );
    }

    /**
     * @Rest\Delete(path="/shops/{id}", requirements={"id": "\d+"},
     *     name="api_shop_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Shop",
     *     description="Delete a single shop",
     *     requirements={
     *         {"name"="id","dataType"="integer","requirement"="\d+","description"="Shop id"}
     *     },
     *     statusCodes={
     *         204="No Content: The server has fulfilled and resulted in a resource being deleted.",
     *         404="Not Found: The resource was not found",
     *     },
     *     views={"default", "v1", "version1", "shops", "delete", "delete-method"},
     *     tags={"v1" = "#48A01D", "DELETE" = "#A01D27"},
     * )
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_SUPER_ADMIN' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )->find( $id );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop',
                'message'     => 'Institute shop was not found',
            ], 404 );
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove( $shop );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Shop $shop
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Shop $shop, bool $isNew = false ) {

        $form = $this->createForm( ShopType::class, $shop );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $shop );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_shop_read', [
                    'version' => 'v1',
                    'id'      => $shop->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }}
