<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\GeneralSpec;
use AppBundle\Form\Type\GeneralSpecType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GeneralSpecController extends FOSRestController {

    private $entityFields = ['specs_class', 'source', 'video_site', 'video', 'description', 'k_j_m_l58_l_k_j87_hl_k_j879', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/products/{productId}/generalspecs/{id}", requirements={"productId": "\d+", "id": "\d+"},
     *     name="api_generalspec_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="GeneralSpec",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $productId, int $id ) {

        $product = $this->getDoctrine()->getRepository( 'AppBundle:Product' )
            ->find( $productId );
        if ( !$product ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product',
                'message'     => 'Product was not found',
            ], 404 );
        }

        $generalspec = $this->getDoctrine()->getRepository( 'AppBundle:CategoryProduct' )
            ->getProductGeneralSpec( $productId, $id );
        if ( !$generalspec ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_generalspec',
                'message'     => 'Product generalspec was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $generalspec->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $generalspec, 200 ) );
    }

    /**
     * @Rest\Post(path="/products/{productId}/generalspecs", requirements={"productId": "\d+"},
     *     name="api_generalspec_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="GeneralSpec",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @return Response
     */
    public function newAction( Request $request, int $productId ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $generalspec = new GeneralSpec();
        $generalspec->setProduct( $result->getProduct() );

        return $this->processForm( $request, $generalspec, true );
    }

    /**
     * @Rest\Route(path="/products/{productId}/generalspecs/{id}", requirements={"productId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_generalspec_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="GeneralSpec",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "GeneralSpecs"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $productId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $generalspec = $this->getDoctrine()->getRepository( 'AppBundle:GeneralSpec' )
            ->getProductGeneralSpec( $productId, $id );
        if ( !$generalspec ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_generalspec',
                'message'     => 'Product generalspec was not found',
            ], 404 );
        }

        return $this->processForm( $request, $generalspec, false );
    }

    /**
     * @Rest\Delete(path="/products/{productId}/generalspecs/{id}", requirements={"productId": "\d+","id": "\d+"},
     *     name="api_generalspec_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="GeneralSpec",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "GeneralSpecs"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $productId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $productId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $generalspec = $this->getDoctrine()->getRepository( 'AppBundle:GeneralSpec' )
            ->getProductGeneralSpec( $productId, $id );
        if ( !$generalspec ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.product_generalspec',
                'message'     => 'Product generalspec was not found',
            ], 404 );
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove( $generalspec );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param GeneralSpec $generalspec
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, GeneralSpec $generalspec, bool $isNew = false ) {

        $form = $this->createForm( GeneralSpecType::class, $generalspec );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $generalspec );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_generalspec_read', [
                    'version' => 'v1',
                    'productId'   => $generalspec->getProduct()->getId(),
                    'id'      => $generalspec->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }}
