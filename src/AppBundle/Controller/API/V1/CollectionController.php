<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\Collection;
use AppBundle\Form\Shop\CollectionType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CollectionController extends FOSRestController {

    private $entityFields = [ 'category', 'parent', 'shop', 'name', 'product_nb', 'level', 'any_parent', 'date' ];

    /*
     *     filters={
     *         {"name"="a-filter", "dataType"="integer"},
     *         {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *     },
     */
    /**
     * @Rest\Get(path="/shops/{shopId}/collections/{id}", requirements={"shopId": "\d+", "id": "\d+"},
     *     name="api_collection_read", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Collection",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @param int $id
     * @return Response
     */
    public function getAction( Request $request, int $shopId, int $id ) {

        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )
            ->find( $shopId );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop',
                'message'     => 'Shop was not found',
            ], 404 );
        }

        $collection = $this->getDoctrine()->getRepository( 'AppBundle:Collection' )
            ->getShopCollection( $shopId, $id );
        if ( !$collection ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop_collection',
                'message'     => 'Shop collection was not found',
            ], 404 );
        }

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $fields as $field ) {

                if ( in_array( $field, $this->entityFields ) ) {
                    $result[ $field ] = $collection->getField( $field );
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $collection, 200 ) );
    }

    /**
     * @Rest\Get(path="/shops/{shopId}/collections", requirements={"shopId": "\d+"},
     *     name="api_collection_read_all", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Collection",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @return Response
     */
    public function cgetAction( Request $request, int $shopId ) {

        $limit = $request->query->getInt( 'limit', 20 );
        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )->find( $shopId );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop',
                'message'     => 'Shop was not found',
            ], 404 );
        }

        $collections = $this->getDoctrine()->getRepository( 'AppBundle:Collection' )
            ->getShopCollections( $shopId );

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $collections as $collection ) {

                $result[ $collection->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $collection->getId() ][ $field ] = $collection->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $collections, 200 ) );
    }

    /**
     * @Rest\Get(path="/shops/{shopSlug}/collections", name="api_collection_read_all_by_slug",
     *     options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Collection",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $shopSlug
     * @return Response
     */
    public function cgetByShopSlugAction( Request $request, string $shopSlug ) {

        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )->findOneBy( [ 'slug' => $shopSlug ] );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop',
                'message'     => 'Shop was not found',
            ], 404 );
        }

        $limit = $request->query->getInt( 'limit', 20 );
        $collections = $this->getDoctrine()->getRepository( 'AppBundle:Collection' )
            ->getCollectionsByShopSlug( $shopSlug );

        $fields = $request->query->get( 'fields' );
        if ( $fields ) {

            $result = [];
            foreach ( $collections as $collection ) {

                $result[ $collection->getId() ] = [];
                foreach ( $fields as $field ) {

                    if ( in_array( $field, $this->entityFields ) ) {
                        $result[ $collection->getId() ][ $field ] = $collection->getField( $field );
                    }
                }
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $collections, 200 ) );
    }

    /**
     * @Rest\Get(path="/shops/{shopId}/collections/{id}/products", requirements={"shopId": "\d+", "id": "\d+"},
     *     name="api_collection_read_products", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Collection",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @return Response
     */
    public function cgetProductsAction( Request $request, int $shopId, int $id ) {

        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )->find( $shopId );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop',
                'message'     => 'Shop was not found',
            ], 404 );
        }

        $collection = $this->getDoctrine()->getRepository( 'AppBundle:Collection' )
            ->getShopCollection( $shopId, $id );
        if ( !$collection ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop_collection',
                'message'     => 'Shop collection was not found',
            ], 404 );
        }

        $products = $this->getDoctrine()->getRepository( 'AppBundle:CollectionProduct' )->findBy( [ 'collection' => $collection ] );
        $result = [ 'collection' => $collection, 'products' => [] ];
        foreach ($products as $collectionProduct) {

            array_push( $result['products'], $collectionProduct );
        }

        return $this->handleView( $this->view( $result, 200 ) );
    }

    /**
     * @Rest\Get(path="/shops/{shopSlug}/collections/{id}/products", requirements={"id": "\d+"},
     *     name="api_collection_read_products_slug", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Collection",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $shopSlug
     * @param int $id
     * @return JsonResponse|Response
     */
    public function cgetProductsByShopSlugAction( Request $request, string $shopSlug, int $id ) {

        $shop = $this->getDoctrine()->getRepository( 'AppBundle:Shop' )->findOneBy( [ 'slug' => $shopSlug ] );
        if ( !$shop ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop',
                'message'     => 'Shop was not found',
            ], 404 );
        }

        $limit = $request->query->getInt( 'limit', 20 );
        $collection = $this->getDoctrine()->getRepository( 'AppBundle:Collection' )
            ->getCollectionByShopSlug( $shopSlug, $id );
        if ( !$collection ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop_collection',
                'message'     => 'Shop collection was not found',
            ], 404 );
        }

        $products = $this->getDoctrine()->getRepository( 'AppBundle:CollectionProduct' )->findBy( [ 'collection' => $collection ] );
        $result = [ 'collection' => $collection, 'products' => [] ];
        foreach ($products as $collectionProduct) {

            array_push( $result['products'], $collectionProduct );
        }

        return $this->handleView( $this->view( $result, 200 ) );
    }

    /**
     * @Rest\Post(path="/shops/{shopId}/collections", requirements={"shopId": "\d+"},
     *     name="api_collection_create", options={ "method_prefix" = false })
     * @ApiDoc(
     *     section="Collection",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @return Response
     */
    public function newAction( Request $request, int $shopId ) {

        $result = $this->get( 'abo.access_checker' )->checkForShop( $shopId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $collection = new Collection();
        $collection->setShop( $result->getShop() );

        return $this->processForm( $request, $collection, true );
    }

    /**
     * @Rest\Route(path="/shops/{shopId}/collections/{id}", requirements={"shopId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_collection_update",options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Collection",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Collections"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $shopId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $shopId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $collection = $this->getDoctrine()->getRepository( 'AppBundle:Collection' )
            ->getShopCollection( $shopId, $id );
        if ( !$collection ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop_collection',
                'message'     => 'Shop collection was not found',
            ], 404 );
        }

        return $this->processForm( $request, $collection );
    }

    /**
     * @Rest\Delete(path="/shops/{shopId}/collections/{id}", requirements={"shopId": "\d+","id": "\d+"},
     *     name="api_collection_delete", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *     section="Collection",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Collections"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $shopId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $shopId, int $id ) {

        $result = $this->get( 'abo.access_checker' )->checkForUser( 'ROLE_USER' );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $collection = $this->getDoctrine()->getRepository( 'AppBundle:Collection' )
            ->getShopCollection( $shopId, $id );
        if ( !$collection ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.shop_collection',
                'message'     => 'Shop collection was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $colProds = $this->getDoctrine()->getRepository( 'AppBundle:CollectionProduct' )->findBy( [ 'collection' => $collection ] );
        foreach ( $colProds as $colProd ) {

            $em->remove( $colProd );
        }

        $em->remove( $collection );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Collection $collection
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Collection $collection, bool $isNew = false ) {

        $form = $this->createForm( CollectionType::class, $collection );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $collection );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_collection_read', [
                    'version' => 'v1',
                    'shopId'  => $collection->getShop()->getId(),
                    'id'      => $collection->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}
